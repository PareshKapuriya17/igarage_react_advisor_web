export const AppSettings = {
  APP_NAME: "iGarage",
  //Development Server
  SERVER_IMAGE_URL: "http://139.59.4.237/uploads/",
  API_BASE_URL: "http://139.59.4.237/api/",
  SOCKET_BASE_URL: `ws://139.59.4.237`,

  //Development Live Server
  // SERVER_IMAGE_URL: "http://139.59.45.121/uploads/",
  // API_BASE_URL: "http://139.59.45.121/api/"
  // SOCKET_BASE_URL: `ws://139.59.45.121`

  //Live Client Server
  // SERVER_IMAGE_URL: "http://139.59.83.117/uploads/",
  // API_BASE_URL: "http://139.59.83.117/api/"
  // SOCKET_BASE_URL: `ws://139.59.83.117/`
};
