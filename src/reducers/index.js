import {combineReducers} from 'redux';
import {connectRouter} from 'connected-react-router'
import { sessionReducer } from 'redux-react-session';
import Settings from './Settings';
import Auth from './Auth';
import Common from './Common';


export default (history) => combineReducers({
  router: connectRouter(history),
  settings: Settings,
  auth: Auth,
  commonData: Common,
  session: sessionReducer
});
