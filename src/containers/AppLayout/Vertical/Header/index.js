import React from "react";
import {withRouter} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import {COLLAPSED_DRAWER, FIXED_DRAWER} from "constants/ActionTypes";
import { toggleCollapsedNav} from "actions/Setting";
import UserInfo from "components/UserInfo";

const Index = (props) => {

  const dispatch = useDispatch();
  const {drawerType} = useSelector(({settings}) => settings);

  const onToggleCollapsedNav = (e) => {
    const val = !props.navCollapsed;
    dispatch(toggleCollapsedNav(val));
  };

  const drawerStyle = drawerType.includes(FIXED_DRAWER) ? "d-block d-xl-none" : drawerType.includes(COLLAPSED_DRAWER) ? "d-block" : "d-none";

  return (
    <AppBar className="app-main-header">
      <Toolbar className="app-toolbar" disableGutters={false}>

        <IconButton className={`jr-menu-icon mr-3 ${drawerStyle}`} aria-label="Menu"
                    onClick={onToggleCollapsedNav}>
          <span className="menu-icon"/>
        </IconButton>

        <ul className="header-notifications list-inline ml-auto">
          <li className="list-inline-item">
          <UserInfo />
            {/* <Tooltip title={"Sign out"}>
              <IconButton color="primary" onClick={() => {dispatch(userSignOut())}}>
                <i className="zmdi zmdi-sign-in zmdi-hc-fw text-white"/>
              </IconButton>
            </Tooltip> */}
          </li>
        </ul>

        <div className="ellipse-shape"/>
      </Toolbar>
    </AppBar>
  );
};


export default withRouter(Index);
