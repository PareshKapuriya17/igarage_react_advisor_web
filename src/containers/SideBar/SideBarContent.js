import React from 'react';
import CustomScrollbars from 'util/CustomScrollbars';
import Navigation from "../../components/Navigation";

const SideBarContent = () => {
  const navigationMenus = [
    {
      name: 'sidebar.main',
      type: 'section',
      children: [
        {
          name: 'Dashboard',
          type: 'item',
          link: '/app/dashboard',
          icon: 'view-dashboard'
        },
        {
          name: 'Reports',
          type: 'item',
          link: '/app/reports',
          icon: 'view-dashboard'
        },
      ]
    },
  ];

  return (
    <CustomScrollbars className=" scrollbar">
      <Navigation menuItems={navigationMenus}/>
    </CustomScrollbars>
  );
};

export default SideBarContent;
