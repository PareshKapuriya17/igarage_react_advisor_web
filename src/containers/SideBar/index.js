import React from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {withRouter} from 'react-router-dom';
import Drawer from '@material-ui/core/Drawer';
import {COLLAPSED_DRAWER, FIXED_DRAWER, HORIZONTAL_NAVIGATION} from 'constants/ActionTypes';
import { toggleCollapsedNav } from 'actions/Setting';
import SideBarContent from "./SideBarContent";
import { Link } from '@material-ui/core';

const SideBar = () => {
  const dispatch = useDispatch();
  const {navCollapsed, drawerType,  navigationStyle} = useSelector(({settings}) => settings);

  // useEffect(() => {
  //   window.addEventListener('resize', () => {
  //     dispatch(updateWindowWidth(window.innerWidth))
  //   });
  // }, []);

  const onToggleCollapsedNav = (e) => {
    const val = !navCollapsed;
    dispatch(toggleCollapsedNav(val));
  };

  let drawerStyle = drawerType.includes(FIXED_DRAWER) ? 'd-xl-flex' : drawerType.includes(COLLAPSED_DRAWER) ? '' : 'd-flex';
  let type = 'permanent';
  if (drawerType.includes(COLLAPSED_DRAWER) || (drawerType.includes(FIXED_DRAWER) && window.innerWidth < 1200)) {
    type = 'temporary';
  }

  if (navigationStyle === HORIZONTAL_NAVIGATION) {
    drawerStyle = '';
    type = 'temporary';
  }
  return (
    <div className={`app-sidebar d-none ${drawerStyle}`}>
      <Drawer className="app-sidebar-content"
              variant={type}
              open={type.includes('temporary') ? navCollapsed : true}
              onClose={onToggleCollapsedNav}
              classes={{
                paper: 'side-nav',
              }}
      >
        <div className="user-profile d-flex flex-row align-items-center justify-content-center p-2">
          <Link className="app-logo" to="/">
            <img src={require('assets/images/logo.png')} alt="iGarage" title="iGarage"/>
          </Link>
        </div>
        <SideBarContent/>
      </Drawer>
    </div>
  );
};


export default withRouter(SideBar);

