import React, {useEffect, useState} from 'react';
import {Link} from 'react-router-dom'
import {useDispatch, useSelector} from 'react-redux';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import IntlMessages from 'util/IntlMessages';
import {userSignIn} from 'actions/Auth';
import CircularProgress from "@material-ui/core/CircularProgress";
import {NotificationContainer, NotificationManager} from "react-notifications";
import { Formik } from 'formik';
import * as Yup from 'yup';

const SignIn = (props) => {

  const [email] = useState('');
  const [password] = useState('');
  const dispatch = useDispatch();
  const token = useSelector(({auth}) => auth.token);
  const {loading, message} = useSelector(({commonData}) => commonData);

  useEffect(() => {
    if (token !== '') {
      props.history.push('/');
    }
  }, [token,props.history]);

  const handleClickSubmit = (values) => {
    dispatch(userSignIn({...values}));
  }

  return (
    <div
      className="app-login-container d-flex justify-content-center align-items-center animated slideInUpTiny animation-duration-3">
      <div className="app-login-main-content">

        <div className="app-logo-content d-flex align-items-center justify-content-center">
          <Link className="logo-lg" to="/" title="iGarage">
            <img src={require("assets/images/logo.png")} alt="iGarage" title="iGarage"/>
          </Link>
        </div>

        <div className="app-login-content">
          <div className="app-login-header mb-4 text-center">
            <h1>Login to your account</h1>
          </div>

          <div className="app-login-form">
            <Formik
              initialValues={{ email:email, password:password }}
              onSubmit={handleClickSubmit}
              validationSchema={Yup.object().shape({
                email: Yup.string().required('Please enter email').email('Please enter valid email'),
                password: Yup.string().required('Please enter password')
              })}
            >
              {(props) => {
                const {
                  values,
                  touched,
                  errors,
                  handleChange,
                  handleBlur,
                  handleSubmit,
                } = props;
                return(
                  <form noValidate onSubmit={handleSubmit}>
                    <fieldset>
                      <TextField
                        autoFocus
                        value={values.email}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        variant="outlined"
                        name="email"
                        label={<IntlMessages id="appModule.email"/>}
                        fullWidth
                        error={errors.email && touched.email}
                        helperText={(errors.email && touched.email) && errors.email}
                        margin="dense"
                        className="mt-1 my-sm-3"
                      />
                      <TextField
                        variant="outlined"
                        type="password"
                        value={values.password}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="password"
                        label={<IntlMessages id="appModule.password"/>}
                        fullWidth
                        error={errors.password && touched.password}
                        helperText={(errors.password && touched.password) && errors.password}
                        margin="dense"
                        className="mt-1 my-sm-3"
                      />

                      <div className="my-3 d-flex align-items-center justify-content-between">
                        <Button fullWidth  type="submit" variant="contained" color="primary">
                          <IntlMessages id="appModule.signIn"/>
                        </Button>
                      </div>

                    </fieldset>
                  </form>
                )
              }}
            </Formik>
          </div>
        </div>
      </div>

      {
        loading &&
        <div className="loader-view">
          <CircularProgress/>
        </div>
      }
      {message && NotificationManager.error(message)}
      <NotificationContainer/>

    </div>
  );
};


export default SignIn;
