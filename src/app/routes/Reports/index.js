import React from "react"
import ApiRequests from "util/Requests";
import { NotificationManager, NotificationContainer } from "react-notifications";
import { TablePagination, Card, CircularProgress } from "@material-ui/core";
import { TableRowCard } from "app/Components/Orders/card-row";
import { CardBody } from "reactstrap";
import { SearchComponent } from "app/Components/Orders/SearchComponent";
import ModelOrderDetail from "app/Components/Orders/orderDetailModel";

export class Report extends React.Component{
    constructor(props){
        super(props);
        this.state = {
          tabPosition: 0,
          selectedStatus: ["completed","canceled"],
          pagination: {total:0,page:0,perPage:10},
          tableList:[],
          listGarage: [],
          listMobileVan: [],
          isLoading: true,
          isModelLoading: false,
          selectedOrder: null,
          isGaragePopup: false,
          isConfirmModel: false,
          isUploadImageModel: false,
          isMobileVanPopup: false,
          isDetailModel: false,
          uploadImageType: "",
          confirmMessage: "",
          confirmType: "",
          isGlobalSearch: false
        }
      }

    componentDidMount(){
        this.getGarageList();
        this.getOrderList();
    }
    
    handleGloableSearch = (data) => {
        let pagination = this.state.pagination;
        pagination.page = 0;
        this.setState({isGlobalSearch: true,pagination,isLoading:true},() => {
          this.getOrderList(data)
        })
    }
    
    clearSearch = () => {
        let pagination = this.state.pagination;
        pagination.page = 0;
        this.setState({isGlobalSearch: false,pagination,isLoading:true},() => {
          this.getOrderList()
        })
    }

    getOrderList = (data) => {
        let params = new FormData();
        if(this.state.isGlobalSearch){
          debugger
          data?.order_id && params.set(`order_id`,data?.order_id);
          data?.order_search_garage && params.set(`order_search_garage`,data?.order_search_garage);
          data?.order_from_date && params.set('order_from_date', data?.order_from_date);
          data?.order_to_date && params.set('order_to_date', data?.order_to_date);
          data?.order_garage_id && params.set('order_garage_id', data?.order_garage_id);
        }
        this.state.selectedStatus.forEach((status,index) => {
            params.set(`order_status[${index}]`,status);
        });
        params.set('page', this.state.pagination.page);
        params.set('per_page', this.state.pagination.perPage);
        ApiRequests.getOrderList(params).then((res) => {
          debugger
          if(res.data.status === 1){
            var list = res.data.data.data;
            var pagination = res.data.data;
            let statePagination = this.state.pagination;
            statePagination.total = pagination.total;
            this.setState({tableList:list, isLoading:false, pagination: statePagination});
          }else{
            NotificationManager.error(res.data.message);
            this.setState({pagination: {total:0,page:0,perPage:10},})
          }
        }).catch((err) => {
          NotificationManager.error(err.response.data.message);
          this.setState({pagination: {total:0,page:0,perPage:10}})
        });
    }

    handleChangePage = (event, newPage) => {
        let pagination = this.state.pagination;
        pagination.page = newPage;
        this.setState({pagination,isLoading:true}, () => {
          this.getOrderList()
        });
    }
    
    handleChangeRowsPerPage = (event) => {
        let pagination = this.state.pagination;
        pagination.perPage = parseInt(event.target.value, 10);
        this.setState({pagination,isLoading:true}, () => {
          this.getOrderList()
        });
    }

    getGarageList = (search = "") => {
      let str = `search=${search}`;
      ApiRequests.getGarageList(str).then(res => {
        let list = res.data.data.data;
        this.setState({listGarage: list});
      });
    }

    onHandleRowButtonClick = (type, order) => {
      switch (type) {
        case "VIEW_DETAIL":
          this.orderActionViewDetail(order);
          break;
        default:
          break;
      }
    }

    orderActionViewDetail = (order) => {
      this.setState({isDetailModel: true, selectedOrder: order});
    }

    handleDetailClose = (isForce) => {
      this.setState({isDetailModel: false, selectedOrder: null});
    }

    render(){
        return(
            <div className="app-wrapper">
                <SearchComponent isFromReport={true} onClear={() => {this.clearSearch()}} onSearch={(data) => {this.handleGloableSearch(data)}} listGarage={this.state.listGarage}/>
                    <div className="animated slideInUpTiny animation-duration-3">
                        {this.state.tableList.length !== 0 && <TablePagination
                            component="div"
                            count={this.state.pagination.total}
                            page={this.state.pagination.page}
                            onChangePage={this.handleChangePage}
                            rowsPerPage={this.state.pagination.perPage}
                            onChangeRowsPerPage={this.handleChangeRowsPerPage}
                            // rowsPerPageOptions={[5,10,15]}
                        /> }
          
                        {this.state.tableList.map((row,index) => (
                            <TableRowCard data={row} key={index} onClick={(type,order) => {this.onHandleRowButtonClick(type,order)} } isLast={(this.state.tableList.length - 1) === index}/>
                        ))}
          
                        {this.state.tableList.length !== 0 && <TablePagination
                            component="div"
                            count={this.state.pagination.total}
                            page={this.state.pagination.page}
                            onChangePage={this.handleChangePage}
                            rowsPerPage={this.state.pagination.perPage}
                            onChangeRowsPerPage={this.handleChangeRowsPerPage}
                            // rowsPerPageOptions={[5,10,15]}
                        /> }

                        {this.state.tableList.length === 0 && (
                            <Card className="shadow border-0 my-3" >
                                <CardBody>
                                    <div className='text-center'> No Records Found!</div>
                                </CardBody>
                            </Card>
                        )}
                    </div>
                    <ModelOrderDetail
                      open={this.state.isDetailModel}
                      isLoading={this.state.isModelLoading}
                      order={this.state.selectedOrder}
                      onClose={(isForce) => {this.handleDetailClose(isForce)}}
                    />
                    {
                    this.state.isLoading &&
                        <div className="loader-view">
                            <CircularProgress />
                        </div>
                    }
                <NotificationContainer />
            </div>
        )
    }
}

export default Report