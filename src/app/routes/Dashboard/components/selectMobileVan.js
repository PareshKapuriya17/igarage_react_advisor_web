import React, { useState, useEffect } from 'react';
import { Dialog, DialogTitle, List, ListItem, ListItemText, CircularProgress, FormControl, InputLabel, OutlinedInput, InputAdornment, IconButton } from '@material-ui/core';
import { Row, Col } from 'reactstrap';
import { Search } from '@material-ui/icons'

export const ModelSelectMobileVan = (props) => {
    const [open, setOpen] = useState(props.isOpen || false);
    const [isLoading, setLoading] = useState(props.isLoading);
    const [items, setItems] = useState(props.mobileVanList || []);
    const [txtSearch, setTxtSearch] = useState("");

    useEffect(() => {
        setItems(props.mobileVanList)
        setLoading(props.isLoading)
    },[props.isOpen, props.mobileVanList,props.isLoading]);

    useEffect(() => {
        setOpen(props.isOpen)
        setTxtSearch("")
    },[props.isOpen])

    const handleListItemClick = (isCLose, id) => {
        setLoading(false)
        props.onSubmit(isCLose, id);
    }

    return(
        <Dialog onClose={() => {handleListItemClick(true,0)}} open={open} disableBackdropClick fullWidth>
            {isLoading && <div className="model-loader-view"><CircularProgress /></div>}
            <DialogTitle>Select Mobile Van</DialogTitle>
            <div>                    
                <List>
                    <ListItem>
                        <FormControl variant="outlined" size="small" fullWidth>
                            <InputLabel htmlFor="outlined-adornment-password">Search</InputLabel>
                            <OutlinedInput
                                id="outlined-adornment-password"
                                type={'text'}
                                value={txtSearch}
                                onChange={(event) => {setTxtSearch(event.target.value)}}
                                onKeyDown={(event) => {event.key === "Enter" && props.onSearchClick(txtSearch)}}
                                endAdornment={
                                    <InputAdornment position="end">
                                        <IconButton onClick={() => {props.onSearchClick(txtSearch)}}>
                                            <Search />
                                        </IconButton>
                                    </InputAdornment>
                                }
                                labelWidth={50}
                            />
                        </FormControl>
                    </ListItem>
                    {items.length === 0 && 
                        <ListItem>
                            <ListItemText>
                                <div className="text-center text-muted">No records found!</div>
                            </ListItemText>
                        </ListItem>
                    }
                    {items.map(item =>
                        <ListItem button onClick={() => handleListItemClick(false,item.mobile_van_id)} key={item.mobile_van_id}>
                            <ListItemText>
                                <Row>
                                    <Col>
                                        <div className="font-weight-bold text-capitalize">
                                            {`${item.mobile_van_name} (${(item.mobile_van_number).toUpperCase()})`}
                                        </div>
                                        <div className="font-weight-normal text-sm">{item.mobile_van_details}</div>
                                    </Col>
                                    <Col className="text-right">
                                        <div className="text-success">
                                            Available
                                        </div>
                                        <div className="d-inline">
                                            <div className="font-weight-normal text-sm">{`${(item.mobile_van_person_name).toUpperCase()} - (${item.mobile_van_person_mobile})`}</div>
                                        </div>
                                    </Col>
                                </Row>
                            </ListItemText>
                        </ListItem>
                    )}
                </List>
            </div>
        </Dialog>
    )
}