import React, { useState, useEffect } from 'react';
import { Dialog, DialogTitle, CircularProgress, DialogContent, DialogActions, Button, FormControl, InputLabel, OutlinedInput } from '@material-ui/core';
import { Row, Col } from 'reactstrap';
import ImageDrpoZone from 'components/Dropzone/Singleimage'

export const UploadImageModel = (props) => {
    const [isLoading, setLoading] = useState(props.isLoading);
    const [files, setFiles] = useState([]);
    const [comment, setComment] = useState('');

    const handleSubmitClick = (isClose) => {
        let data = {
            comment: comment,
            files: files
        }
        props.onSubmit(isClose, props.type, data);
    }

    useEffect(() => {
        setLoading(props.loading)
    },[props.loading])

    return(
        <Dialog open={true} disableBackdropClick fullWidth >
            {isLoading && <div className="model-loader-view"><CircularProgress /></div>}
            <DialogTitle>Upload Images</DialogTitle>
            <DialogContent>
                <Row>
                    <Col md={12} className="mb-2">
                        <FormControl variant="outlined" size="small" fullWidth>
                            <InputLabel htmlFor="outlined-adornment-password">Comment</InputLabel>
                            <OutlinedInput
                                id="outlined-adornment-password"
                                type={'text'}
                                multiline
                                rows={3}
                                value={comment}
                                onChange={(event) => {setComment(event.target.value)}}
                                labelWidth={70}
                            />
                        </FormControl>
                    </Col>
                    <Col md={12}>
                        <ImageDrpoZone onChange={(files) => {setFiles(files)}} multiple={true} />
                    </Col>
                </Row>
            </DialogContent>
            <DialogActions>
                <Button onClick={() => {handleSubmitClick(true)}} color="secondary" variant="outlined" >
                    Cancel
                </Button>
                <Button onClick={() => {handleSubmitClick(false)}} color="primary"  variant="outlined">
                    Ok
                </Button>
            </DialogActions>
        </Dialog>
    )
}