import React, { useState } from 'react';
import { AppBar, Tabs, Tab } from "@material-ui/core";

export const TopNavigation = (props) => {
    const [position, setPosition] = useState(0);

    const handleChange = (event, value) => {
        setPosition(value);
        props.handleChangeTab(value);
    }
    return (
        <div className="w-100">
            <AppBar position="static" color="primary">
            <Tabs
                value={position}
                onChange={handleChange}
                indicatorColor="secondary"
                textColor="inherit"
                variant="scrollable"
                scrollButtons="on"
            >
                <Tab label="New Requests"/>
                <Tab label="Assigned"/>
                <Tab label="Accepted"/>
                <Tab label="Pick Up/Self Drop"/>
                <Tab label="In Progress"/>
                <Tab label="Job Done"/>
                <Tab label="Delivered"/>
                <Tab label="Completed"/>
            </Tabs>
            </AppBar>
        </div>
    )
}