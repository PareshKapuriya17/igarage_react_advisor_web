import React, { useState, useEffect } from "react";
import { Dialog, DialogTitle, DialogContent, DialogActions, Button, FormControl, InputLabel, OutlinedInput, CircularProgress } from "@material-ui/core";
import { Col, Row } from "reactstrap";
import { APP_CONST } from "constants/AppConsts";

export const ModelConfirm = (props) => {
    const [txtReason, setTxtReason] = useState('');
    const [txtSuggestion, setTxtSuggestion] = useState('');
    const [visibleArea, setVisibleArea] = useState(0);
    const [isLoading, setLoading] = useState(props.isLoading);

    useEffect(() => {
        setLoading(props.isLoading);
    },[props.isLoading])

    const handleClose = (okPress) => {
        let data = null;
        if(okPress){
            if(props.type === APP_CONST.ORDER_BUTTONS_IDENTIFIER.BTN_CANCEL){
                if(visibleArea === 0){
                    setVisibleArea(1)
                }else{
                    data = txtReason
                    props.onClose(okPress, props.type, data)
                }
            }else if(props.type === APP_CONST.ORDER_BUTTONS_IDENTIFIER.BTN_NOT_AVAILABLE){
                if(visibleArea === 0){
                    setVisibleArea(2)
                }else{
                    data = txtSuggestion
                    props.onClose(okPress, props.type, data)
                }
            }
            else{
                props.onClose(okPress, props.type, data)
            }
        }else{
            props.onClose(okPress, props.type, data)
        }
    }

    const getHeaderText = (area) => {
        switch (area) {
            case 0:
                return "Are you sure?"
            case 1:
                return "Cancel Reason."
            case 2:
                return "If you have any suggetion here."
            default:
                return ""
        }
    }

    return(
        <Dialog
            disableBackdropClick
            disableEscapeKeyDown
            maxWidth="xs"
            onClose={() => {handleClose(false)}}
            open={props.isOpen}
            fullWidth
        >
            {isLoading && <div className="model-loader-view"><CircularProgress /></div>}
            <DialogTitle>{getHeaderText(visibleArea)}</DialogTitle>
            <DialogContent>
                <Row>
                    {visibleArea === 0 && (<Col>{props.message}</Col>)}
                    {visibleArea === 1 && (
                        <Col>
                            <FormControl variant="outlined" size="small" fullWidth>
                                <InputLabel htmlFor="outlined-adornment-password">Cancel Reason</InputLabel>
                                <OutlinedInput
                                    id="outlined-adornment-password"
                                    type={'text'}
                                    multiline
                                    rows={3}
                                    value={txtReason}
                                    onChange={(event) => {setTxtReason(event.target.value)}}
                                    // onKeyDown={(event) => {event.key === "Enter" && props.onSearchClick(txtSearch)}}
                                    labelWidth={110}
                                    autoFocus
                                />
                            </FormControl>
                        </Col>
                    )}
                    {visibleArea === 2 && (
                        <Col>
                            <FormControl variant="outlined" size="small" fullWidth>
                                <InputLabel htmlFor="outlined-adornment-password">Suggestion</InputLabel>
                                <OutlinedInput
                                    id="outlined-adornment-password"
                                    type={'text'}
                                    multiline
                                    rows={3}
                                    value={txtSuggestion}
                                    onChange={(event) => {setTxtSuggestion(event.target.value)}}
                                    // onKeyDown={(event) => {event.key === "Enter" && props.onSearchClick(txtSearch)}}
                                    labelWidth={80}
                                    autoFocus
                                />
                            </FormControl>
                        </Col>
                    )}
                </Row>
            </DialogContent>
            <DialogActions>
                <Button onClick={() => {handleClose(false)}} color="secondary" variant="outlined">
                    Cancel
                </Button>
                <Button onClick={() => {handleClose(true)}} color="primary" disabled={visibleArea === 1 && txtReason.length === 0} variant="outlined">
                    Ok
                </Button>
            </DialogActions>
        </Dialog>
    )
}