import React, { useState, useEffect } from 'react';
import { Dialog, DialogTitle, List, ListItem, ListItemText, Avatar, ListItemAvatar, CircularProgress, FormControl, InputLabel, OutlinedInput, InputAdornment, IconButton } from '@material-ui/core';
import { Row, Col, Label } from 'reactstrap';
import Rating from 'react-rating';
import { StarRate, Search } from '@material-ui/icons'

export const ModelSelectGarage = (props) => {
    const [open, setOpen] = useState(props.isOpen || false);
    const [isLoading, setLoading] = useState(props.isLoading);
    const [items, setItems] = useState(props.garageList || []);
    const [txtSearch, setTxtSearch] = useState("");

    useEffect(() => {
        setItems(props.garageList)
        setLoading(props.isLoading)
    },[props.isOpen, props.garageList,props.isLoading]);

    useEffect(() => {
        setOpen(props.isOpen)
        setTxtSearch("")
    },[props.isOpen])

    const handleListItemClick = (isCLose, garageId) => {
        setLoading(false)
        props.onSubmit(isCLose, garageId);
    }

    return(
        <Dialog onClose={() => {handleListItemClick(true,0)}} open={open} disableBackdropClick fullWidth>
            {isLoading && <div className="model-loader-view"><CircularProgress /></div>}
            <DialogTitle>Select Garage</DialogTitle>
            <div>                    
                <List>
                    <ListItem>
                        <FormControl variant="outlined" size="small" fullWidth>
                            <InputLabel htmlFor="outlined-adornment-password">Search</InputLabel>
                            <OutlinedInput
                                id="outlined-adornment-password"
                                type={'text'}
                                value={txtSearch}
                                onChange={(event) => {setTxtSearch(event.target.value)}}
                                onKeyDown={(event) => {event.key === "Enter" && props.onSearchClick(txtSearch)}}
                                endAdornment={
                                    <InputAdornment position="end">
                                        <IconButton onClick={() => {props.onSearchClick(txtSearch)}}>
                                            <Search />
                                        </IconButton>
                                    </InputAdornment>
                                }
                                labelWidth={50}
                            />
                        </FormControl>
                    </ListItem>
                    {items.length === 0 && 
                        <ListItem>
                            <ListItemText>
                                <div className="text-center text-muted">No records found!</div>
                            </ListItemText>
                        </ListItem>
                    }
                    {items.map(item =>
                        <ListItem button onClick={() => handleListItemClick(false,item.garage_detail_id)} key={item.garage_detail_id}>
                            <ListItemAvatar>
                                <Avatar alt="Remy Sharp" src={item.user_profile_image}/>
                            </ListItemAvatar>
                            <ListItemText>
                                <Row>
                                    <Col>
                                        <div className="font-weight-bold">
                                            {item.garage_name}
                                        </div>
                                        <div className="font-weight-normal text-sm">({item.garage_address} - {item.garage_pincode})</div>
                                    </Col>
                                    <Col className="text-right">
                                        <div className="text-success">
                                            Available
                                        </div>
                                            <div className="d-inline">
                                                <Label className="text-muted" style={{fontSize: '.7rem'}}>{((5 * item.garage_accept_rate) / 100).toFixed(1)}</Label>
                                                <Rating 
                                                    readonly 
                                                    initialRating={(5 * item.garage_accept_rate) / 100}
                                                    emptySymbol={<StarRate fontSize="small" color="disabled"/>} 
                                                    fullSymbol={[1,2,3,4,5].map(n => <StarRate fontSize="small" color="primary" />)}
                                                />
                                            </div>
                                    </Col>
                                </Row>
                            </ListItemText>
                        </ListItem>
                    )}
                </List>
            </div>
        </Dialog>
    )
}