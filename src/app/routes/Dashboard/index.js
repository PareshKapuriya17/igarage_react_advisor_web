import React from "react";
import { TopNavigation } from "./components/topNavigation";
import { Card, CircularProgress, TablePagination } from "@material-ui/core";
import { CardBody } from "reactstrap";
import ApiRequests from "util/Requests";
import { TableRowCard } from "app/Components/Orders/card-row";
import { APP_CONST } from "constants/AppConsts";
import { ModelSelectGarage } from "./components/selectGarage";
import { ModelSelectMobileVan } from "./components/selectMobileVan";
import {
  NotificationManager,
  NotificationContainer,
} from "react-notifications";
import { ModelConfirm } from "./components/confirmModel";
import { UploadImageModel } from "./components/uploadImageModel";
import ModelOrderDetail from "../../Components/Orders/orderDetailModel";
import { SearchComponent } from "../../Components/Orders/SearchComponent";

const order_status = [
  ["advisor_assigned", "no_time_slot"],
  ["garage_assigned", "garage_rejected", "no_time_slot"],
  ["garage_accepted"],
  ["pickup_dropoff", "estimated", "estimated_accepted", "estimated_rejected"],
  [
    "in_progress",
    "in_progress_estimated",
    "in_progress_estimated_accepted",
    "in_progress_estimated_rejected",
  ],
  ["done"],
  ["delivered"],
  ["completed"],
];

class SamplePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tabPosition: 0,
      selectedStatus: order_status[0],
      pagination: { total: 0, page: 0, perPage: 10 },
      tableList: [],
      listGarage: [],
      listMobileVan: [],
      isLoading: true,
      isModelLoading: false,
      selectedOrder: null,
      isGaragePopup: false,
      isConfirmModel: false,
      isUploadImageModel: false,
      isMobileVanPopup: false,
      isDetailModel: false,
      uploadImageType: "",
      confirmMessage: "",
      confirmType: "",
      isGlobalSearch: false,
    };
  }

  componentDidMount() {
    this.getGarageList();
    this.getOrderList();
  }

  handleChangeTab = (tab) => {
    debugger;
    let pagination = this.state.pagination;
    pagination.page = 0;
    this.setState(
      {
        tabPosition: tab,
        selectedStatus: order_status[tab],
        isLoading: true,
        pagination,
      },
      () => {
        this.getOrderList();
      }
    );
  };

  handleGloableSearch = (data) => {
    let pagination = this.state.pagination;
    pagination.page = 0;
    this.setState({ isGlobalSearch: true, pagination, isLoading: true }, () => {
      this.getOrderList(data);
    });
  };

  clearSearch = () => {
    let pagination = this.state.pagination;
    pagination.page = 0;
    this.setState(
      { isGlobalSearch: false, pagination, isLoading: true },
      () => {
        this.getOrderList();
      }
    );
  };

  getOrderList = (data) => {
    debugger;
    let params = new FormData();
    if (this.state.isGlobalSearch) {
      debugger;
      data?.order_id && params.set(`order_id`, data?.order_id);
      data?.order_search_garage &&
        params.set(`order_search_garage`, data?.order_search_garage);
      data?.order_from_date &&
        params.set("order_from_date", data?.order_from_date);
      data?.order_to_date && params.set("order_to_date", data?.order_to_date);
      data?.order_status &&
        order_status[data?.order_status].forEach((status, index) => {
          params.set(`order_status[${index}]`, status);
        });
      data?.order_garage_id &&
        params.set("order_garage_id", data?.order_garage_id);
    } else {
      this.state.selectedStatus.forEach((status, index) => {
        params.set(`order_status[${index}]`, status);
      });
    }

    params.set("page", this.state.pagination.page);
    params.set("per_page", this.state.pagination.perPage);
    ApiRequests.getOrderList(params)
      .then((res) => {
        debugger;
        if (res.data.status === 1) {
          var list = res.data.data.data;
          var pagination = res.data.data;
          let statePagination = this.state.pagination;
          statePagination.total = pagination.total;
          this.setState({
            tableList: list,
            isLoading: false,
            pagination: statePagination,
          });
        } else {
          NotificationManager.error(res.data.message);
          this.setState({ pagination: { total: 0, page: 0, perPage: 10 } });
        }
      })
      .catch((err) => {
        NotificationManager.error(err.response.data.message);
        this.setState({ pagination: { total: 0, page: 0, perPage: 10 } });
      });
  };

  handleChangePage = (event, newPage) => {
    let pagination = this.state.pagination;
    pagination.page = newPage;
    this.setState({ pagination, isLoading: true }, () => {
      this.getOrderList();
    });
  };

  handleChangeRowsPerPage = (event) => {
    let pagination = this.state.pagination;
    pagination.perPage = parseInt(event.target.value, 10);
    this.setState({ pagination, isLoading: true }, () => {
      this.getOrderList();
    });
  };

  onHandleRowButtonClick = (type, order) => {
    switch (type) {
      case APP_CONST.ORDER_BUTTONS_IDENTIFIER.BTN_SELECT_GARAGE:
        this.orderActionSelectGarage(order);
        break;
      case APP_CONST.ORDER_BUTTONS_IDENTIFIER.BTN_SELECT_MOBILE_VAN:
        this.orderActionSelectMobileVan(order);
        break;
      case APP_CONST.ORDER_BUTTONS_IDENTIFIER.BTN_CANCEL:
        this.orderActionCancel(order);
        break;
      case APP_CONST.ORDER_BUTTONS_IDENTIFIER.BTN_NOT_AVAILABLE:
        this.orderActionNotAvailable(order);
        break;
      case APP_CONST.ORDER_BUTTONS_IDENTIFIER.BTN_UPLOAD_IMAGES:
        this.orderActionUploadImages(order);
        break;
      case APP_CONST.ORDER_BUTTONS_IDENTIFIER.BTN_COMPLETE:
        this.orderActionComplate(order);
        break;
      case APP_CONST.ORDER_BUTTONS_IDENTIFIER.BTN_MARK_AS_PATYMENT:
        this.orderActionMarkAsPayment(order);
        break;
      case "VIEW_DETAIL":
        this.orderActionViewDetail(order);
        break;
      default:
        break;
    }
  };

  orderActionViewDetail = (order) => {
    this.setState({ isDetailModel: true, selectedOrder: order });
  };

  handleDetailClose = (isForce) => {
    if (isForce) {
      this.getOrderList();
    }
    this.setState({ isDetailModel: false, selectedOrder: null });
  };

  orderActionSelectGarage = (order) => {
    this.setState(
      { isGaragePopup: true, selectedOrder: order, isModelLoading: true },
      () => {
        this.getGarageList();
      }
    );
  };

  orderActionSelectMobileVan = (order) => {
    this.setState(
      { isMobileVanPopup: true, selectedOrder: order, isModelLoading: true },
      () => {
        this.getMobileVanList();
      }
    );
  };

  orderActionCancel = (order) => {
    this.setState({
      isConfirmModel: true,
      selectedOrder: order,
      confirmType: APP_CONST.ORDER_BUTTONS_IDENTIFIER.BTN_CANCEL,
      confirmMessage: `Do you want to Cancel #${order.order_id} order?`,
    });
  };

  orderActionNotAvailable = (order) => {
    this.setState({
      isConfirmModel: true,
      selectedOrder: order,
      confirmType: APP_CONST.ORDER_BUTTONS_IDENTIFIER.BTN_NOT_AVAILABLE,
      confirmMessage: `Do you want to mark this #${order.order_id} order as a Not Available for service?`,
    });
  };

  orderActionComplate = (order) => {
    this.setState({
      isConfirmModel: true,
      selectedOrder: order,
      confirmType: APP_CONST.ORDER_BUTTONS_IDENTIFIER.BTN_COMPLETE,
      confirmMessage: `Do you want to complete this #${order.order_id} order?`,
    });
  };

  orderActionUploadImages = (order) => {
    this.setState({
      isUploadImage: true,
      uploadImageType:
        order.order_status === "garage_accepted" ? "PICKUP" : "DROPOFF",
      selectedOrder: order,
    });
  };

  orderActionMarkAsPayment = (order) => {
    this.setState({
      isConfirmModel: true,
      selectedOrder: order,
      confirmType: APP_CONST.ORDER_BUTTONS_IDENTIFIER.BTN_MARK_AS_PATYMENT,
      confirmMessage: `Do you want to mark #${order.order_id} order as a Payment complete?`,
    });
  };

  getGarageList = (search = "") => {
    //garage_pincode=${pin}
    this.setState({ isModelLoading: true }, () => {
      let str = `search=${search}`;
      ApiRequests.getGarageList(str).then((res) => {
        debugger;
        if (res.data.data) {
          let list = res.data.data.data;
          this.setState({ listGarage: list, isModelLoading: false });
        }
      });
    });
  };

  getMobileVanList = (search = "") => {
    this.setState({ isModelLoading: true }, () => {
      let str = `search=${search}`;
      ApiRequests.getMobileVanList(str).then((res) => {
        let list = res.data.data.data;
        this.setState({ listMobileVan: list, isModelLoading: false });
      });
    });
  };

  assignGarageOrMobileVan = (isClose, id) => {
    if (isClose) {
      this.setState({
        isGaragePopup: false,
        isMobileVanPopup: false,
        selectedOrder: null,
        isModelLoading: false,
      });
    } else {
      this.setState({ isModelLoading: true }, () => {
        var params = new FormData();
        params.set("order_id", this.state.selectedOrder.order_id);
        params.set("garage_detail_id", id);
        ApiRequests.assignGarageMobileVan(params)
          .then((res) => {
            if (res.data.status === 1) {
              this.setState({
                isModelLoading: false,
                isGaragePopup: false,
                isMobileVanPopup: false,
                selectedOrder: null,
              });
              this.getOrderList();
              NotificationManager.success("Order Assigned", "Order Updated");
            } else {
              this.setState({ isModelLoading: false });
              NotificationManager.error(res.data.message);
            }
          })
          .catch((err) => {
            this.setState({ isModelLoading: false });
            NotificationManager.error(err.response.data.message);
          });
      });
    }
  };

  cancelOrder = (reason) => {
    this.setState({ isModelLoading: true }, () => {
      let params = new FormData();
      params.set("order_id", this.state.selectedOrder.order_id);
      params.set("cancel_reason", reason);
      ApiRequests.orderCancel(params)
        .then((res) => {
          if (res.data.status === 1) {
            this.setState({
              isModelLoading: false,
              isConfirmModel: false,
              selectedOrder: null,
            });
            this.getOrderList();
            NotificationManager.success("Order Cancelled.", "Order Updated");
          } else {
            this.setState({ isModelLoading: false });
            NotificationManager.error(res.data.message);
          }
        })
        .catch((err) => {
          this.setState({ isModelLoading: false });
          NotificationManager.error(err.response.data.message);
        });
    });
  };

  notAvailableOrder = (suggetion = "") => {
    this.setState({ isModelLoading: true }, () => {
      let params = new FormData();
      params.set("order_id", this.state.selectedOrder.order_id);
      params.set("time_slot_suggestion", suggetion);
      ApiRequests.timeSlotNotAvailable(params)
        .then((res) => {
          if (res.data.status === 1) {
            this.setState({
              isModelLoading: false,
              isConfirmModel: false,
              selectedOrder: null,
            });
            this.getOrderList();
            NotificationManager.success(
              "Marked as a time slot not available.",
              "Order Updated"
            );
          } else {
            this.setState({ isModelLoading: false });
            NotificationManager.error(res.data.message);
          }
        })
        .catch((err) => {
          this.setState({ isModelLoading: false });
          NotificationManager.error(err.response.data.message);
        });
    });
  };

  handleConfirmClose = (isOkClick, type, data) => {
    if (isOkClick) {
      switch (type) {
        case APP_CONST.ORDER_BUTTONS_IDENTIFIER.BTN_CANCEL:
          this.cancelOrder(data);
          break;
        case APP_CONST.ORDER_BUTTONS_IDENTIFIER.BTN_NOT_AVAILABLE:
          this.notAvailableOrder(data);
          break;
        case APP_CONST.ORDER_BUTTONS_IDENTIFIER.BTN_COMPLETE:
          this.completeOrder(data);
          break;
        case APP_CONST.ORDER_BUTTONS_IDENTIFIER.BTN_MARK_AS_PATYMENT:
          this.markAsPaymentOrder(data);
          break;

        default:
          break;
      }
    } else {
      this.setState({
        isConfirmModel: false,
        isModelLoading: false,
        confirmMessage: "",
        confirmType: "",
        order: null,
      });
    }
  };

  handleUploadImage = (isClose, type, data) => {
    if (isClose) {
      this.setState({
        isUploadImage: false,
        uploadImageType: "",
        selectedOrder: null,
      });
    } else {
      this.setState({ isModelLoading: true }, () => {
        let params = new FormData();
        params.set("order_id", this.state.selectedOrder.order_id);
        params.set("order_image_note", data.comment);
        data.files.forEach((item, index) => {
          params.set(`order_images[${index}]`, item);
        });
        if (type === "PICKUP") {
          ApiRequests.orderPickupImages(params)
            .then((res) => {
              if (res.data.status === 1) {
                this.setState({
                  isModelLoading: false,
                  isUploadImage: false,
                  uploadImageType: "",
                  selectedOrder: null,
                });
                this.getOrderList();
                NotificationManager.success(
                  "Pickup Has Been Completed",
                  "Order Updated"
                );
              } else {
                this.setState({ isModelLoading: false });
                NotificationManager.error(res.data.message);
              }
            })
            .catch((err) => {
              this.setState({ isModelLoading: false });
              NotificationManager.error(err.response.data.message);
            });
        } else {
          ApiRequests.orderDeliveryImages(params)
            .then((res) => {
              if (res.data.status === 1) {
                ApiRequests.orderDelivered(this.state.selectedOrder.order_id)
                  .then((res) => {
                    if (res.data.status === 1) {
                      this.setState({
                        isModelLoading: false,
                        isUploadImage: false,
                        uploadImageType: "",
                        selectedOrder: null,
                      });
                      this.getOrderList();
                      NotificationManager.success(
                        "Order has been Delivered .",
                        "Order Updated"
                      );
                    } else {
                      this.setState({ isModelLoading: false });
                      NotificationManager.error(res.data.message);
                    }
                  })
                  .catch((err) => {
                    this.setState({ isModelLoading: false });
                    NotificationManager.error(err.response.data.message);
                  });
              } else {
                this.setState({ isModelLoading: false });
                NotificationManager.error(res.data.message);
              }
            })
            .catch((err) => {
              this.setState({ isModelLoading: false });
              NotificationManager.error(err.response.data.message);
            });
        }
      });
    }
  };

  markAsPaymentOrder = () => {
    this.setState({ isModelLoading: true }, () => {
      ApiRequests.orderCompletes(this.state.selectedOrder.order_id)
        .then((res) => {
          if (res.data.status === 1) {
            this.setState({
              isModelLoading: false,
              isConfirmModel: false,
              selectedOrder: null,
            });
            this.getOrderList();
            NotificationManager.success(
              "Marked as payment completed.",
              "Order Updated"
            );
          } else {
            this.setState({ isModelLoading: false });
            NotificationManager.error(res.data.message);
          }
        })
        .catch((err) => {
          this.setState({ isModelLoading: false });
          NotificationManager.error(err.response.data.message);
        });
    });
  };

  completeOrder = () => {
    this.setState({ isModelLoading: true }, () => {
      ApiRequests.orderCompletes(this.state.selectedOrder.order_id)
        .then((res) => {
          if (res.data.status === 1) {
            this.setState({
              isModelLoading: false,
              isConfirmModel: false,
              selectedOrder: null,
            });
            this.getOrderList();
            NotificationManager.success("Order Complated.", "Order Updated");
          } else {
            this.setState({ isModelLoading: false });
            NotificationManager.error(res.data.message);
          }
        })
        .catch((err) => {
          this.setState({ isModelLoading: false });
          NotificationManager.error(err.response.data.message);
        });
    });
  };

  render() {
    return (
      <div className="app-wrapper">
        <SearchComponent
          onClear={() => {
            this.clearSearch();
          }}
          onSearch={(data) => {
            this.handleGloableSearch(data);
          }}
          listGarage={this.state.listGarage}
        />
        <div className="d-flex justify-content-center">
          {!this.state.isGlobalSearch && (
            <TopNavigation
              handleChangeTab={(tab) => {
                this.handleChangeTab(tab);
              }}
            />
          )}
        </div>
        <div className="animated slideInUpTiny animation-duration-3">
          {this.state.tableList.length !== 0 && (
            <TablePagination
              component="div"
              count={this.state.pagination.total}
              page={this.state.pagination.page}
              onChangePage={this.handleChangePage}
              rowsPerPage={this.state.pagination.perPage}
              onChangeRowsPerPage={this.handleChangeRowsPerPage}
              // rowsPerPageOptions={[5,10,15]}
            />
          )}

          {!this.state.isLoading &&
            this.state.tableList.map((row, index) => (
              <TableRowCard
                data={row}
                key={index}
                onClick={(type, order) => {
                  this.onHandleRowButtonClick(type, order);
                }}
                isLast={this.state.tableList.length - 1 === index}
              />
            ))}

          {this.state.tableList.length !== 0 && (
            <TablePagination
              component="div"
              count={this.state.pagination.total}
              page={this.state.pagination.page}
              onChangePage={this.handleChangePage}
              rowsPerPage={this.state.pagination.perPage}
              onChangeRowsPerPage={this.handleChangeRowsPerPage}
              // rowsPerPageOptions={[5,10,15]}
            />
          )}
          {this.state.tableList.length === 0 && (
            <Card className="shadow border-0 my-3">
              <CardBody>
                <div className="text-center"> No Records Found!</div>
              </CardBody>
            </Card>
          )}
        </div>
        {this.state.isLoading && (
          <div className="loader-view">
            <CircularProgress />
          </div>
        )}
        <NotificationContainer />
        <ModelSelectGarage
          isOpen={this.state.isGaragePopup}
          isLoading={this.state.isModelLoading}
          order={this.state.selectedOrder}
          garageList={this.state.listGarage}
          onSearchClick={(val) => {
            this.getGarageList(val);
          }}
          onSubmit={(isClose, param) => {
            this.assignGarageOrMobileVan(isClose, param);
          }}
        />
        <ModelSelectMobileVan
          isOpen={this.state.isMobileVanPopup}
          isLoading={this.state.isModelLoading}
          order={this.state.selectedOrder}
          mobileVanList={this.state.listMobileVan}
          onSearchClick={(val) => {
            this.getMobileVanList(val);
          }}
          onSubmit={(isClose, param) => {
            this.assignGarageOrMobileVan(isClose, param);
          }}
        />
        {this.state.isConfirmModel && (
          <ModelConfirm
            isOpen={this.state.isConfirmModel}
            isLoading={this.state.isModelLoading}
            message={this.state.confirmMessage}
            type={this.state.confirmType}
            onClose={(isOkClick, type, data) => {
              this.handleConfirmClose(isOkClick, type, data);
            }}
          />
        )}
        <ModelOrderDetail
          open={this.state.isDetailModel}
          isLoading={this.state.isModelLoading}
          order={this.state.selectedOrder}
          onClose={(isForce) => {
            this.handleDetailClose(isForce);
          }}
        />
        {this.state.isUploadImage && (
          <UploadImageModel
            onSubmit={(isClose, type, data) => {
              this.handleUploadImage(isClose, type, data);
            }}
            type={this.state.uploadImageType}
          />
        )}
      </div>
    );
  }
}

export default SamplePage;
