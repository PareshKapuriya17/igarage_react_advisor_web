import React from 'react';
import {Route, Switch, withRouter} from 'react-router-dom';
import asyncComponent from '../../util/asyncComponent';

const Routes = ({match}) =>
  <Switch>
    <Route path={`${match.url}/dashboard`} component={asyncComponent(() => import('./Dashboard'))}/>
    <Route path={`${match.url}/reports`} component={asyncComponent(() => import('./Reports'))}/>
    <Route path={`${match.url}/profile`} component={asyncComponent(() => import('./../../components/Profile-iGarage'))}/>
    <Route path={`${match.url}/change-password`} component={asyncComponent(() => import('./../../components/Change-password'))}/>
    <Route component={asyncComponent(() => import("app/routes/extraPages/routes/404"))}/>
  </Switch>;


export default withRouter(Routes);

