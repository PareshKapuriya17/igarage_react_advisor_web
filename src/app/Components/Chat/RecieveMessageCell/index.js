import React from "react";
import moment from "moment";
import { AppSettings } from "constants/AppSettings";
import { Avatar } from "@material-ui/core";
import { PictureAsPdfTwoTone } from "@material-ui/icons";

const ReceivedMessageCell = ({
  conversation,
  user_image,
  customerimage,
  garageprofileimage,
}) => {
  debugger;
  let image = customerimage !== undefined ? customerimage : garageprofileimage;
  return (
    <div className="d-flex flex-nowrap chat-item">
      <Avatar src={image} className="size-40 align-self-end" />
      <div
        className={
          conversation.message_type === "text" ? "bubble" : "bubble p-1"
        }
        style={{ overflow: "hidden" }}
      >
        {conversation.message_type === "text" && (
          <div style={{ whiteSpace: "pre-line" }} className="message">
            {conversation.message}
          </div>
        )}
        {conversation.message_type === "image" && (
          <div className="message">
            <a
              href={`${AppSettings.API_BASE_URL}download?file=${conversation.message}`}
              download
              rel="noopener noreferrer"
            >
              <img
                style={{ width: "200px", borderRadius: "16px 16px 16px 0" }}
                src={`${AppSettings.SERVER_IMAGE_URL}${conversation.message}`}
                alt={conversation.message}
              />
            </a>
          </div>
        )}
        {conversation.message_type === "pdf" && (
          <div className="message">
            <a
              href={`${AppSettings.API_BASE_URL}download?file=${conversation.message}`}
              download
              rel="noopener noreferrer"
            >
              <PictureAsPdfTwoTone
                fontSize="large"
                style={{ fontSize: "6.1875rem" }}
              />{" "}
            </a>
          </div>
        )}
        <div className="time text-muted text-left" style={{ fontSize: ".7em" }}>
          {moment(conversation.message_created_at).format("LT")}
        </div>
      </div>
    </div>
  );
};

export default ReceivedMessageCell;
