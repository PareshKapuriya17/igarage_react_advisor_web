import React from "react";
import autoscroll from "autoscroll-react";
import FlatList from "flatlist-react";
import moment from "moment";
import SentMessageCell from "../SentMessageCell";
import ReceivedMessageCell from "../RecieveMessageCell";

class ChatList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  renderMessage = (msg, idx) => {
    const isFromCurrentUser =
      msg.from_user_id === this.props.currentUser.user_id;
    if (isFromCurrentUser) {
      return (
        <SentMessageCell
          key={idx}
          conversation={msg}
          user_image={this.props.currentUser.user_profile_image}
          customerimage={this.props.customerimage}
          advisorprofileimage={this.props.advisorprofileimage}
        />
      );
    } else {
      debugger;
      return (
        <ReceivedMessageCell
          key={idx}
          conversation={msg}
          user_image={this.props.toUser.user_profile_image}
          customerimage={this.props.customerimage}
          garageprofileimage={this.props.garageprofileimage}
        />
      );
    }
  };

  groupSeparator = (group, idx, groupLabel) => {
    debugger;
    return <hr className="hr-text" data-content={groupLabel} />;
  };

  render() {
    return (
      <div
        style={{ height: "calc(50vh - 45px)", overflowY: "scroll" }}
        {...this.props}
      >
        <FlatList
          list={this.props.messages}
          renderItem={this.renderMessage}
          renderWhenEmpty={() => (
            <div className="text-center mt-2">List is empty!</div>
          )}
          sortBy={["message_created_at"]}
          groupBy={(chat) => moment(chat.message_created_at).format("LL")}
          groupSeparator={this.groupSeparator}
        />
      </div>
    );
  }
}

export default autoscroll(ChatList, { isScrolledDownThreshold: 100 });
