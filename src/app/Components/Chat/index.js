import React from "react";
import { Row, Col, CardBody } from "reactstrap";
import {
  Card,
  FormControl,
  OutlinedInput,
  IconButton,
} from "@material-ui/core";
import ApiRequests from "util/Requests";
import SentMessageCell from "./SentMessageCell";
import ReceivedMessageCell from "./RecieveMessageCell";
import { Send, AttachFile } from "@material-ui/icons";
import { WebSocket } from "util/socket";
import ChatList from "./List";

// a global variable so we can disconnect once we unmount
let subscription;
let subscriptiong;

export class Chat extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      roomNumber: props.roomNumber,
      messages: [],
      order: props.order,
      currentUser: props.currentUser,
      toUser: props.toUser,
      perPage: 10,
      txtMessage: "",
      isMoreHistory: true,
      errors: {},
    };

    this.wb = new WebSocket();
    this.wg = new WebSocket();
  }

  componentDidMount() {
    debugger;
    this.wb.connect();
    this.wg.connect();
    if (this.props.role === "customer") {
      subscription = this.wb.subscribe(
        this.state.roomNumber,
        this.handleMessageAdd,
        this.props.role
      );
    } else {
      subscriptiong = this.wg.subscribegarage(
        this.state.roomNumber,
        this.handleMessageAdd,
        this.props.role
      );
    }
    this.getChatHistory();
  }

  handleMessageAdd = (message) => {
    const { type, data } = message;
    debugger;
    // you could handle various types here, like deleting or editing a message
    let index = this.state.messages.findIndex(function(single) {
      return single.message_id === data.message_id;
    });
    if (index === -1) {
      switch (type) {
        case "newMessage":
          this.setState((prevState) => ({
            messages: [...prevState.messages, data],
          }));
          break;
        default:
      }
    }
  };

  componentWillUnmount() {
    debugger;
    // this.wb.close()();
    // this.wg.close()();
    // subscription.close();
    // subscriptiong.close();
  }

  getChatHistory = () => {
    const offset = this.state.messages.length;
    debugger;
    const params = `order_id=${this.state.order.order_id}&to_user_id=${this.state.toUser.user_id}&offset=${offset}&per_page=${this.state.perPage}`;
    console.log(this.state.order.order_id);
    ApiRequests.getChatHistory(params).then((data) => {
      debugger;
      if (data.data.status === 1) {
        const length = data.data.data.length;
        if (length === 0) {
          this.setState({ isMoreHistory: false });
        } else {
          this.setState((prevState) => ({
            messages: [...prevState.messages, ...data.data.data],
          }));
        }
      }
    });
  };

  getNewHistory = () => {
    if (this.state.isMoreHistory) {
      this.getChatHistory(this.state.messages.length);
    }
  };

  renderMessage = (msg, idx) => {
    const isFromCurrentUser =
      msg.from_user_id === this.state.currentUser.user_id;
    if (isFromCurrentUser) {
      return (
        <SentMessageCell
          key={idx}
          conversation={msg}
          user_image={this.state.currentUser.user_profile_image}
          advisorprofileimage={this.props.advisorprofileimage}
        />
      );
    } else {
      debugger;
      return (
        <ReceivedMessageCell
          key={idx}
          conversation={msg}
          user_image={this.state.toUser.user_profile_image}
          customerimage={this.props.customerimage}
          garageprofileimage={this.props.garageprofileimage}
        />
      );
    }
  };
  sendMessage = (message, type = "text") => {
    if (this.props.role === "customer") {
      subscription = this.wb.subscribe(
        this.state.roomNumber,
        this.handleMessageAdd,
        this.props.role
      );
    } else {
      subscriptiong = this.wg.subscribegarage(
        this.state.roomNumber,
        this.handleMessageAdd,
        this.props.role
      );
    }
    debugger;
    var length = message.trim().length;
    if (message === "" || length === 0) {
      this.setState({
        txtMessage: "",
      });
      return;
    }
    const params = {
      order_id: this.state.order.order_id,
      from_user_id: this.state.currentUser.user_id,
      to_user_id: this.state.toUser.user_id,
      message: message,
      message_type: type,
    };
    if (this.props.role === "customer") {
      subscription.emit("message", params);
      this.setState({ txtMessage: "" });
    } else {
      subscriptiong.emit("message", params);
      this.setState({ txtMessage: "" });
    }
  };

  onClickAddFile = () => {
    debugger;
    this._refFileInput.click();
  };

  onChangeFile = (event) => {
    if (event.target.files.length === 0) {
      return;
    }
    debugger;
    const name = event.target.files[0].name;
    const ext = name.split(".").pop();
    let validate = this.validation(event.target.files[0]);
    if (validate) {
      const params = new FormData();
      params.set("chat_file", event.target.files[0]);
      ApiRequests.uploadChatFile(params).then((data) => {
        if (data.data.status === 1) {
          this.sendMessage(data.data.data, ext === "pdf" ? "pdf" : "image");
        }
      });
    }
  };
  validation = (data) => {
    debugger;
    let errors = {};
    let formIsValid = true;
    if (data.type !== "image/jpeg") {
      if (data.type !== "image/png") {
        if (data.type !== "image/svg+xml") {
          if (data.type !== "application/pdf") {
            formIsValid = false;
            errors["Upload"] = "You can Only Upload JPG PNG Or PDF file";
          }
        }
      }
    }
    this.setState({
      errors: errors,
    });
    return formIsValid;
  };
  render() {
    return (
      <Row className="mb-3">
        <Col>
          <Card className={`shadow border-0 `}>
            <CardBody className="p-0">
              <div style={{ height: "50vh" }}>
                <ChatList
                  currentUser={this.state.currentUser}
                  toUser={this.state.toUser}
                  messages={this.state.messages}
                  customerimage={this.props.customerimage}
                  advisorprofileimage={this.props.advisorprofileimage}
                  garageprofileimage={this.props.garageprofileimage}
                  onScrolledTop={() => {
                    this.getNewHistory();
                  }}
                />
                <FormControl variant="outlined" size="small" fullWidth>
                  <OutlinedInput
                    id="outlined-adornment-password"
                    type={"text"}
                    multiline
                    value={this.state.txtMessage}
                    onChange={(e) => {
                      this.setState({ txtMessage: e.target.value });
                    }}
                    placeholder="Enter message here..."
                    onKeyUp={(e) => {
                      e.key === "Enter" &&
                        !e.shiftKey &&
                        this.sendMessage(this.state.txtMessage);
                    }}
                    endAdornment={
                      <IconButton
                        size="small"
                        onClick={() => {
                          this.sendMessage(this.state.txtMessage);
                        }}
                      >
                        <Send fontSize="inherit" />
                      </IconButton>
                    }
                    startAdornment={
                      <IconButton
                        size="small"
                        onClick={() => {
                          this.onClickAddFile();
                        }}
                      >
                        <AttachFile fontSize="inherit" />
                      </IconButton>
                    }
                  />
                </FormControl>
                <input
                  ref={(ref) => (this._refFileInput = ref)}
                  type="file"
                  onChange={this.onChangeFile}
                  accept=".png,.jpeg,.pdf"
                  className="d-none"
                />
              </div>
              {this.state.errors.Upload ? (
                <div
                  className="errorMsg "
                  style={{
                    fontSize: "12px",
                    color: "red",
                    fontweight: "600",
                  }}
                >
                  {this.state.errors.Upload}
                </div>
              ) : null}
            </CardBody>
          </Card>
        </Col>
      </Row>
    );
  }
}
