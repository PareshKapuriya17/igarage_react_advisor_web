import React from "react";
import { Row, Col } from "reactstrap";
import {
  FormControl,
  InputLabel,
  OutlinedInput,
  TextField,
  Button,
  ButtonGroup,
} from "@material-ui/core";
import { Autocomplete } from "@material-ui/lab";
import { Search, Clear } from "@material-ui/icons";

export class SearchComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      txtSearch: "",
      order_status: "",
      order_from_date: "",
      order_to_date: "",
      order_garage_id: "",
      order_search_garage: "",
      order_id: "",
      order_Garage: {
        lable: "",
        value: "",
      },
      order_status_autocomplete: {
        lable: "",
        value: "",
      },
      listGarage: [],
      isSearch: false,
      isFromReport: props.isFromReport || false,
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ ...nextProps });
  }

  handleSearch = () => {
    let data = this.state;
    delete data.listGarage;
    delete data.isSearch;
    this.props.onSearch(data);
    this.setState({ isSearch: true });
  };

  handleClear = () => {
    this.setState(
      {
        isSearch: false,
        order_id: "",
        txtSearch: "",
        order_status: "",
        order_from_date: "",
        order_to_date: "",
        order_garage_id: "",
        order_search_garage: "",
        order_Garage: {
          lable: "",
          value: "",
        },
        order_status_autocomplete: {
          lable: "",
          value: "",
        },
      },
      () => {
        this.props.onClear();
      }
    );
  };

  isValidSearch = () => {
    const {
      order_status,
      order_from_date,
      order_to_date,
      order_garage_id,
      order_search_garage,
      order_id,
    } = this.state;
    if (
      order_status === "" &&
      order_from_date === "" &&
      order_to_date === "" &&
      order_garage_id === "" &&
      order_search_garage === "" &&
      order_id === ""
    ) {
      return false;
    }
    return true;
  };

  handleTextSearchInput = (val = "") => {
    debugger;
    let isnum = /^\d+$/.test(val);
    let order_id = "";
    let order_search_garage = "";
    if (isnum) {
      order_id = val;
    } else {
      order_search_garage = val;
    }
    this.setState({ txtSearch: val, order_id, order_search_garage });
  };

  render() {
    const garageList = this.state.listGarage.map((item) => {
      return { lable: item.garage_name, value: item.order_garage_id };
    });
    const statusList = [
      { lable: "New Requests", value: 0 },
      { lable: "Assigned", value: 1 },
      { lable: "Accepted", value: 2 },
      { lable: "Pick Up/Self Drop", value: 3 },
      { lable: "In Progress", value: 4 },
      { lable: "Job Done", value: 5 },
      { lable: "Delivered", value: 6 },
      { lable: "Completed", value: 7 },
    ];
    return (
      <Row md={3} sm={2} lg={4} xs={1} xl={6} className="mb-3">
        <Col className={"mb-3"}>
          <FormControl variant="outlined" size="small" fullWidth>
            <InputLabel htmlFor="outlined-adornment-password">
              Search
            </InputLabel>
            <OutlinedInput
              id="outlined-adornment-password"
              type={"text"}
              value={this.state.txtSearch}
              onChange={(event) => {
                this.handleTextSearchInput(event.target.value);
              }}
              placeholder="Search Order Id, Garage Name..."
              labelWidth={50}
            />
          </FormControl>
        </Col>
        <Col className={"mb-3"}>
          <FormControl variant="outlined" size="small" fullWidth>
            <InputLabel shrink={true} htmlFor="outlined-adornment-password">
              From Date
            </InputLabel>
            <OutlinedInput
              id="outlined-adornment-date"
              type={"date"}
              notched={true}
              value={this.state.order_from_date}
              onChange={(event) => {
                this.setState({ order_from_date: event.target.value });
              }}
              labelWidth={75}
            />
          </FormControl>
        </Col>
        <Col className={"mb-3"}>
          <FormControl variant="outlined" size="small" fullWidth>
            <InputLabel shrink={true} htmlFor="outlined-adornment-password">
              To Date
            </InputLabel>
            <OutlinedInput
              id="outlined-adornment-date"
              type={"date"}
              notched={true}
              value={this.state.order_to_date}
              onChange={(event) => {
                this.setState({ order_to_date: event.target.value });
              }}
              labelWidth={55}
              InputLabelProps={{
                shrink: true,
              }}
            />
          </FormControl>
        </Col>
        <Col className={"mb-3"}>
          <Autocomplete
            size="small"
            id="combo-box-garage"
            value={{
              lable: this.state.order_Garage.lable,
              value: this.state.order_Garage.value,
            }}
            getOptionSelected={(option) =>
              option.value === this.state.order_Garage.value
            }
            onChange={(event, newValue) => {
              this.setState({
                order_garage_id: newValue ? newValue.value : "",
                order_Garage: {
                  lable: newValue ? newValue.lable : "",
                  value: newValue ? newValue.value : "",
                },
              });
            }}
            options={garageList}
            getOptionLabel={(option) => option.lable}
            renderInput={(params) => (
              <TextField {...params} label="Select Garage" variant="outlined" />
            )}
          />
        </Col>
        {!this.state.isFromReport && (
          <Col className={"mb-3"}>
            <Autocomplete
              size="small"
              id="combo-box-status"
              options={statusList}
              value={{
                lable: this.state.order_status_autocomplete.lable,
                value: this.state.order_status_autocomplete.value,
              }}
              getOptionSelected={(option) =>
                option.value === this.state.order_status_autocomplete.value
              }
              onChange={(event, newValue) => {
                this.setState({
                  order_status: newValue ? newValue.value : "",
                  order_status_autocomplete: {
                    lable: newValue ? newValue.lable : "",
                    value: newValue ? newValue.value : "",
                  },
                });
              }}
              getOptionLabel={(option) => option.lable}
              renderInput={(params) => (
                <TextField
                  {...params}
                  label="Select Status"
                  variant="outlined"
                />
              )}
            />
          </Col>
        )}
        <Col className={"mb-3 p-0"}>
          <ButtonGroup
            color="primary"
            aria-label="outlined primary button group"
            fullWidth
          >
            <Button
              color="primary"
              className=""
              onClick={() => {
                this.handleSearch();
              }}
              disabled={!this.isValidSearch()}
            >
              <Search fontSize="inherit" className="mx-1" /> Search
            </Button>
            {this.state.isSearch && (
              <Button
                color="secondary"
                className=""
                onClick={() => {
                  this.handleClear();
                }}
              >
                <Clear fontSize="inherit" className="mx-1" /> Clear
              </Button>
            )}
          </ButtonGroup>
        </Col>
      </Row>
    );
  }
}
