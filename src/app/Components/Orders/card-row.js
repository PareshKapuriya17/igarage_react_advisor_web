import React, { useState } from "react";
import { Row, Col, Badge, Button } from "reactstrap";
import moment from "moment";
import { APP_CONST } from "constants/AppConsts";

export const GetOrderStatus = ({ order }) => {
  let txtStatus = "";
  switch (order.order_status) {
    case "advisor_assigned":
    case "placed":
      txtStatus = "ORDER PLACED";
      break;
    case "pickup_dropoff":
      txtStatus = order.order_pickup === "pickup" ? "PICK UP" : "SELF DROP";
      break;
    case "estimated":
      txtStatus = "Updated Estimation";
      break;
    case "estimated_rejected":
      txtStatus = "Updated Estimate Rejected";
      break;
    case "estimated_accepted":
      txtStatus = "Updated Estimate Accept";
      break;
    case "in_progress_estimated":
      txtStatus = "Updated Estimation";
      break;
    case "in_progress_estimated_accepted":
      txtStatus = "Updated Estimate Accept";
      break;
    case "in_progress_estimated_rejected":
      txtStatus = "Updated Estimate Rejected";
      break;
    default:
      txtStatus = order.order_status.replace(/_/g, " ").toUpperCase();
      break;
  }
  return (
    <Badge color="primary" className="mb-0">
      {txtStatus}
    </Badge>
  );
};

const RenderItem = (props) => {
  const isAmount = props.isAmount || false;
  return (
    <h5>
      <b>{props.lable} : </b>
      {isAmount && <code className="text-black">&#x20b9;</code>}
      {props.value}
    </h5>
  );
};

export const convertTime = (time) => {
  time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [
    time,
  ];
  if (time.length > 1) {
    time = time.slice(1);
    time[5] = +time[0] < 12 ? " AM" : " PM";
    time[0] = +time[0] % 12 || 12;
  }
  return time.join("");
};

export const getStatusBadge = (order) => {
  if (order.order_door_step === 1) {
    return (
      <Badge color="dark" className="mb-0">
        Door Step
      </Badge>
    );
  } else {
    if (order.order_pickup === "pickup") {
      return (
        <Badge color="warning" className="mb-0">
          Pick Up
        </Badge>
      );
    } else {
      return (
        <Badge color="light" className="mb-0">
          Self Drop
        </Badge>
      );
    }
  }
};

export const TableRowCard = (props) => {
  debugger;
  const [sizeMD, setSizeMD] = useState(4);
  // useEffect(() => {}, [props.data]);
  const RenderActionButtons = (order) => {
    const status = order.order_status;
    const isDoorStep = order.order_door_step === 1;
    const isPickup = order.order_pickup === "pickup";

    const btnSelectGarage = () => (
      <Button
        color={"primary"}
        block={true}
        outline={false}
        onClick={() => {
          props.onClick(
            APP_CONST.ORDER_BUTTONS_IDENTIFIER.BTN_SELECT_GARAGE,
            order
          );
        }}
      >
        Select Garage
      </Button>
    );
    const btnSelectMobileVan = () => (
      <Button
        color={"primary"}
        block={true}
        outline={false}
        onClick={() => {
          props.onClick(
            APP_CONST.ORDER_BUTTONS_IDENTIFIER.BTN_SELECT_MOBILE_VAN,
            order
          );
        }}
      >
        Select Mobile Van
      </Button>
    );
    const btnCancel = () => (
      <Button
        color={"danger"}
        block={true}
        outline={true}
        onClick={() => {
          props.onClick(APP_CONST.ORDER_BUTTONS_IDENTIFIER.BTN_CANCEL, order);
        }}
      >
        Cancel
      </Button>
    );
    const btnNotAvailable = () => (
      <Button
        color={"danger"}
        block={true}
        outline={true}
        onClick={() => {
          props.onClick(
            APP_CONST.ORDER_BUTTONS_IDENTIFIER.BTN_NOT_AVAILABLE,
            order
          );
        }}
      >
        Not Available
      </Button>
    );
    const btnComplate = () => (
      <Button
        color={"success"}
        block={true}
        outline={false}
        onClick={() => {
          props.onClick(APP_CONST.ORDER_BUTTONS_IDENTIFIER.BTN_COMPLETE, order);
        }}
      >
        Complete
      </Button>
    );
    const btnUploadImage = () => (
      <Button
        color={"info"}
        block={true}
        outline={false}
        onClick={() => {
          props.onClick(
            APP_CONST.ORDER_BUTTONS_IDENTIFIER.BTN_UPLOAD_IMAGES,
            order
          );
        }}
      >
        Upload Images
      </Button>
    );
    const btnMarkAsPayment = () => (
      <Button
        color={"success"}
        block={true}
        outline={false}
        onClick={() => {
          props.onClick(
            APP_CONST.ORDER_BUTTONS_IDENTIFIER.BTN_MARK_AS_PATYMENT,
            order
          );
        }}
      >
        Mark as Payment
      </Button>
    );

    switch (status) {
      case "no_time_slot":
        return <div className="">{isDoorStep && btnCancel()}</div>;
      case "advisor_assigned":
        return (
          <div className="">
            {isDoorStep && btnSelectMobileVan()}
            {!isDoorStep && btnSelectGarage()}
            {btnCancel()}
          </div>
        );
      case "garage_assigned":
        return (
          <div className="">
            {isDoorStep && btnSelectMobileVan()}
            {!isDoorStep && btnSelectGarage()}
            {isDoorStep && btnComplate()}
            {isDoorStep && btnNotAvailable()}
            {btnCancel()}
          </div>
        );
      case "garage_rejected":
        return (
          <div className="">
            {isDoorStep && btnSelectMobileVan()}
            {!isDoorStep && btnSelectGarage()}
            {btnCancel()}
          </div>
        );
      case "garage_accepted":
        return (
          <div className="">
            {isPickup && btnUploadImage()}
            {btnCancel()}
          </div>
        );
      case "pickup_dropoff":
        return <div className="">{btnCancel()}</div>;
      case "estimated":
        return <div className="">{btnCancel()}</div>;
      case "estimated_accepted":
        return <div className="">{btnCancel()}</div>;
      case "estimated_rejected":
        return <div className="">{btnCancel()}</div>;
      case "in_progress":
        return <div className="">{btnCancel()}</div>;
      case "in_progress_estimated":
        return <div className="">{btnCancel()}</div>;
      case "in_progress_estimated_accepted":
        return <div className="">{btnCancel()}</div>;
      case "in_progress_estimated_rejected":
        return <div className="">{btnCancel()}</div>;
      case "done":
        return (
          <div className="">
            {isPickup && btnUploadImage()}
            {btnCancel()}
          </div>
        );
      case "delivered":
        return <div className="">{btnMarkAsPayment()}</div>;

      default:
        setSizeMD(3);
        return <React.Fragment></React.Fragment>;
    }
  };

  return (
    <div
      className={`user-list d-flex flex-row card shadow ${props.isLast &&
        "mb-0"}`}
    >
      {/* <Avatar
                alt='...'
                src={`${props.data.order_car_details.vehicle_image}`}
                className="user-avatar"
            /> */}
      <div className="description w-100">
        <Row xs={2} sm={2} md={sizeMD}>
          <Col
            onClick={() => {
              props.onClick("VIEW_DETAIL", props.data);
            }}
            style={{ cursor: "pointer" }}
          >
            <Row className="d-block">
              <Col>
                <RenderItem
                  lable="Order ID"
                  value={`#${props.data.order_id}`}
                />
              </Col>
              <Col>
                <RenderItem
                  lable="Status"
                  value={<GetOrderStatus order={props.data} />}
                />
              </Col>
              <Col>
                <RenderItem
                  lable="Amount"
                  value={`${props.data.order_total.grand_total.toFixed(2)}`}
                  isAmount={true}
                />
              </Col>
            </Row>
          </Col>
          <Col
            onClick={() => {
              props.onClick("VIEW_DETAIL", props.data);
            }}
            style={{ cursor: "pointer" }}
          >
            <Row className="d-block">
              <Col>
                <RenderItem lable="Type" value={getStatusBadge(props.data)} />
              </Col>
              <Col>
                <RenderItem
                  lable="Date"
                  value={`${moment(props.data.order_location.date).format(
                    "DD MMM YYYY"
                  )}`}
                />
              </Col>
              <Col>
                <RenderItem
                  lable="Time"
                  value={`${convertTime(
                    props.data.order_location.start_time
                  )} - ${convertTime(props.data.order_location.end_time)}`}
                />
              </Col>
            </Row>
          </Col>
          <Col
            onClick={() => {
              props.onClick("VIEW_DETAIL", props.data);
            }}
            style={{ cursor: "pointer" }}
          >
            <Row className="d-block">
              <Col>
                <RenderItem
                  lable="Order Date"
                  value={moment(props.data.order_created_at).format(
                    "DD MMM YYYY hh:mm A"
                  )}
                />
              </Col>
              <Col>
                <RenderItem
                  lable="Customer Name"
                  value={props.data.order_customer.user_name}
                />
              </Col>
              <Col>
                {props.data.order_door_step === 1 ? (
                  <RenderItem
                    lable="Mobilevan Name"
                    value={
                      props.data.order_mobile_van?.mobile_van_name || "N/A"
                    }
                  />
                ) : (
                  <RenderItem
                    lable="Garage Name"
                    value={props.data.order_garage?.garage_name || "N/A"}
                  />
                )}
              </Col>
            </Row>
          </Col>
          {sizeMD === 4 && (
            <Col>
              <Row className="">
                <Col>{RenderActionButtons(props.data)}</Col>
              </Row>
            </Col>
          )}
        </Row>

        {/* <h6>{designation}</h6>
                <p className="text-muted">{description}</p> */}
      </div>
    </div>
  );
};
