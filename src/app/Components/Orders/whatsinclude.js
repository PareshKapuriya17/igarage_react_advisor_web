import React from "react";
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  // Button,
  CircularProgress,
  // DialogActions,
} from "@material-ui/core";
// import Slider from "react-slick";
// import {
//   // Container,
//   Row,
//   Col,

//   //   Label,
//   //   CardText,
// } from "reactstrap";
import { Tooltip, IconButton } from "@material-ui/core";
import { CheckCircle } from "@material-ui/icons";
import CloseIcon from "@material-ui/icons/CancelOutlined";
// import Slider from "react-slick";
export default class ConfirmDialog extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: this.props.isVisible || false,
      data: this.props.data,
    };
  }

  handleRequestClose = (is) => {
    this.props.onSubmit(is);
  };

  render() {
    debugger;
    const data =
      this.state.data.description !== undefined
        ? this.state.data.description
        : this.state.data.service_description !== undefined
        ? this.state.data.service_description
        : this.state.data.package_description !== undefined
        ? this.state.data.package_description
        : null;
    return (
      <Dialog
        disableBackdropClick
        open={this.state.open}
        onClose={() => this.handleRequestClose(false)}
        fullWidth={"md"}
      >
        {this.state.isLoading && (
          <div className="model-loader-view">
            <CircularProgress />
          </div>
        )}
        <DialogTitle>
          <div className="d-flex">
            <h3> What's Includes</h3>
            <div className="ml-auto">
              <Tooltip title={"Close"}>
                <IconButton
                  color="primary"
                  className="p-0"
                  onClick={() => this.handleRequestClose(false)}
                >
                  <CloseIcon />
                </IconButton>
              </Tooltip>
            </div>
          </div>
        </DialogTitle>
        <DialogContent>
          <DialogContentText>
            {/* <div className="mb-20">
              <Slider {...settings}>
                {this.state.data.imageurls.map((item, index) => {
                  return (
                    <div className="p-3 image-details" key={index}>
                      <img src={item}></img>
                    </div>
                  );
                })}
              </Slider>
            </div> */}
            {data === "" || data === null ? (
              <div className="mb-20">
                <h5>
                  <div className="text-left  d-inline-flex">
                    <p className="text-justify font-weight-light">
                      No Description Avalabile
                    </p>
                  </div>
                </h5>
              </div>
            ) : (
              <div className="mb-20">
                {data.split("\n").map((str, index) => {
                  return (
                    <h5>
                      <div key={index} className="text-left  d-inline-flex">
                        <div>
                          <CheckCircle
                            className="mr-2 text-success"
                            fontSize="inherit"
                          />
                        </div>
                        <p className="text-justify font-weight-light">{str}</p>
                      </div>
                    </h5>
                  );
                })}
              </div>
            )}
            {/* <div className="mb-20">
              {data.split("\n").map((str, index) => {
                return (
                  <h5>
                    <div key={index} className="text-left  d-inline-flex">
                      <div>
                        <CheckCircle
                          className="mr-2 text-success"
                          fontSize="inherit"
                        />
                      </div>
                      <p className="text-justify font-weight-light">{str}</p>
                    </div>
                  </h5>
                );
              })}
            </div> */}
          </DialogContentText>
        </DialogContent>
      </Dialog>
    );
  }
}
