import React from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import CloseIcon from "@material-ui/icons/Close";
import Slide from "@material-ui/core/Slide";
import WhatsInclude from "./whatsinclude";
import InfoIcon from "@material-ui/icons/Info";
import {
  Row,
  Col,
  CardBody,
  Card,
  CardHeader,
  Badge,
  Collapse,
} from "reactstrap";
import UserProfileCard from "components/dashboard/Common/userProfileCard/UserProfileCard";
import {
  DialogContent,
  DialogTitle,
  Tooltip,
  ButtonGroup,
  List,
  ListItem,
  FormControl,
  InputLabel,
  OutlinedInput,
  InputAdornment,
  ListItemText,
  Avatar,
  ListItemIcon,
  CircularProgress,
} from "@material-ui/core";
import {
  GetOrderStatus,
  convertTime,
  getStatusBadge,
} from "app/Components/Orders/card-row";
import IamageDilog from "./imagedilog";
import moment from "moment";
import { Edit, Cancel, Add, Delete, Search, Save } from "@material-ui/icons";
import ApiRequests from "util/Requests";
import { NotificationManager } from "react-notifications";
import { Chat } from "../Chat";

function Transition(props) {
  return <Slide direction="up" {...props} />;
}

class ModelOrderDetail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: props.open,
      isLoading: props.isLoading,
      order: props.order,
      oldEstimation: [],
      isEditEstimation: false,
      isSelectOptionModel: false,
      selectOptionModelType: "",
      isOptionModelLoading: false,
      isImagedetails: false,
      imagedata: null,
      isInclude: false,
      includesdata: null,
      txtSearch: "",
      listServices: [],
      listPackages: [],
      isForce: false,
      customeService: {
        info: "",
        price: "",
      },
      isAllowedEdit: false,
    };
  }

  componentWillReceiveProps(nextPrpos) {
    let isAllowedEdit = false;
    let allow_status = [
      "pickup_dropoff",
      "estimated",
      "estimated_accepted",
      "estimated_rejected",
      "in_progress",
      "in_progress_estimated",
      "in_progress_estimated_accepted",
      "in_progress_estimated_rejected",
    ];
    if (nextPrpos.order) {
      isAllowedEdit = allow_status.includes(nextPrpos.order.order_status);
    }
    this.setState({
      ...nextPrpos,
      isEditEstimation: false,
      isForce: false,
      isAllowedEdit,
    });
  }
  handleImageConform = (isconform, data) => {
    this.setState({
      isImagedetails: false,
    });
  };
  handleImage = () => {
    this.imageGetApi(this.state.order.order_id);
    // this.setState({
    //   isImagedetails: true,
    // });
  };
  imageGetApi = (id) => {
    let response = null;
    ApiRequests.imageGetApi(id).then((data) => {
      debugger;
      if (Object.keys(data.data.data).length > 0) {
        response = data.data.data;
      }
      debugger;
      if (data.data.status === 1) {
        this.setState({ imagedata: response });
        this.setState({
          isImagedetails: true,
        });
      }
    });
  };
  handleInclude = (data) => {
    console.log(data);
    this.setState({
      isInclude: true,
      includesdata: data,
    });
  };
  handleIncludeClose = () => {
    this.setState({
      isInclude: false,
    });
  };
  handleRequestClose = () => {
    this.props.onClose(this.state.isForce);
  };

  handleRequestCloseOptionModel = () => {
    this.setState({ isSelectOptionModel: false });
  };

  handleCliclEdit = () => {
    let order = this.state.order;
    let oldEstimation = [];
    if (this.state.isEditEstimation) {
      order.order_items = this.state.oldEstimation;
    } else {
      Object.assign(oldEstimation, order.order_items);
    }
    this.setState({
      isEditEstimation: !this.state.isEditEstimation,
      order: order,
      oldEstimation,
    });
  };

  removeItem = (index) => {
    let oldEst = this.state.order;
    oldEst.order_items.splice(index, 1);
    this.setState({ order: oldEst });
  };

  onAddClick = () => {
    this.setState(
      {
        isSelectOptionModel: true,
        isOptionModelLoading: true,
        selectOptionModelType: "SERVICE",
        txtSearch: "",
        customeService: { info: "", price: "" },
      },
      () => {
        this.getList();
      }
    );
  };

  handleOptionBtn = (type) => {
    this.setState(
      {
        selectOptionModelType: type,
        isOptionModelLoading: true,
        txtSearch: "",
      },
      () => {
        this.getList();
      }
    );
  };

  onSearchClick = () => {
    this.setState({ isOptionModelLoading: true }, () => {
      this.getList();
    });
  };

  getPrice = (originalPrice, discount) => {
    let price = originalPrice;
    switch (discount.type.toLowerCase()) {
      case "flat":
        price = price - Number(discount.amount);
        break;
      case "percentage":
        let dis = price / Number(discount.amount);
        price = price - dis;
        break;

      default:
        break;
    }

    return price;
  };

  getList = () => {
    let str = `search=${this.state.txtSearch}`;
    switch (this.state.selectOptionModelType) {
      case "SERVICE":
        ApiRequests.getServiceList(str).then((res) => {
          let list = res.data.data.data;
          this.setState({ listServices: list, isOptionModelLoading: false });
        });
        break;
      case "PACKAGE":
        ApiRequests.getPackageList().then((res) => {
          let list = res.data.data.data;
          this.setState({ listPackages: list, isOptionModelLoading: false });
        });
        break;

      default:
        break;
    }
  };

  handleListItemClick = (
    id,
    name,
    price,
    originalPrice,
    isPackage,
    discription
  ) => {
    debugger;
    let order = this.state.order;
    let newItem = {
      service_id: id,
      name: name,
      price: Number(price),
      originalPrice: originalPrice,
      status: "pending",
      is_package: isPackage,
      description: discription || null,
    };
    order.order_items.push(newItem);
    this.setState({ order });
  };

  updateEstimation = () => {
    debugger;
    this.setState({ isLoading: true }, () => {
      let params = {
        order_id: this.state.order.order_id,
        in_progress: this.state.order.order_status.includes("in_progress")
          ? "yes"
          : "no",
        order_items: this.state.order.order_items,
        order_discount: this.state.order.order_discount,
        order_total: {
          items_total: 0,
          tax: 0,
          grand_total: 0,
        },
      };

      let itemTotal = params.order_items.reduce(
        (total, item) =>
          total + (item.originalPrice ? item.originalPrice : item.price),
        0
      );
      let grandTotal = params.order_items.reduce(
        (total, item) => total + item.price,
        0
      );
      params.order_total.items_total = itemTotal;
      params.order_total.grand_total = grandTotal;
      params.order_items.forEach((item) => {
        delete item.originalPrice;
      });
      debugger;
      ApiRequests.updateEstimation(params).then((res) => {
        if (res.data.status === 1) {
          ApiRequests.sendEstimation(params.order_id).then((res) => {
            if (res.data.status === 1) {
              let order = this.state.order;
              order.order_items = params.order_items;
              this.setState({
                isLoading: false,
                order,
                isForce: true,
                isEditEstimation: !this.state.isEditEstimation,
              });
              NotificationManager.success(
                "New Estimation updated and sent for approval",
                "Order Updated"
              );
            } else {
              this.setState({ isLoading: false });
              NotificationManager.error(res.data.message);
            }
          });
        } else {
          this.setState({ isModelLoading: false });
          NotificationManager.error(res.data.message);
        }
      });
    });
  };

  isItemAdded = (id, isPackage) => {
    let is = false;
    this.state.order.order_items.forEach((item) => {
      if (item.service_id === id && item.is_package === isPackage) {
        is = true;
        return;
      }
    });
    return is;
  };

  render() {
    debugger;
    const { order } = this.state;
    if (!this.state.open) {
      return <div></div>;
    }
    return (
      <div>
        <Dialog
          fullScreen
          open={this.state.open}
          onClose={this.handleRequestClose}
          TransitionComponent={Transition}
        >
          {this.state.isLoading && (
            <div className="loader-view">
              <CircularProgress />
            </div>
          )}
          <AppBar className="position-relative">
            <Toolbar>
              <IconButton onClick={this.handleRequestClose} aria-label="Close">
                <CloseIcon color="" style={{ color: "#ffffff" }} />
              </IconButton>
              <Typography variant="title" color="inherit" style={{ flex: 1 }}>
                ORDER DETAILS (#{order.order_id})
              </Typography>
              <Button
                onClick={this.handleRequestClose}
                style={{ color: "#ffffff" }}
              >
                Close
              </Button>
            </Toolbar>
          </AppBar>
          <DialogContent>
            <Row>
              <Col md={3}>
                <UserProfileCard
                  user={order.order_customer}
                  address={order.order_location}
                  userType="CUSTOMER"
                  customerimage={order.customer_profile_image}
                />
                <Chat
                  roomNumber={`order_${order.order_id}_advisor_customer`}
                  order={order}
                  currentUser={order.order_advisor}
                  toUser={order.order_customer}
                  role="customer"
                  customerimage={order.customer_profile_image}
                  advisorprofileimage={order.advisor_profile_image}
                />
              </Col>
              <Col md={6}>
                <Row>
                  <Col md={12} xs={12} lg={12} sm={12}>
                    <Card className={`shadow border-0 `}>
                      <CardHeader className="bg-primary text-white text-center">
                        {getStatusBadge(order)}
                      </CardHeader>
                      <CardBody>
                        <Row>
                          <Col
                            md={4}
                            sm={12}
                            xs={12}
                            className="order-1 order-md-1 order-sm-2 order-xs-2 order-lg-1 "
                          >
                            <Row>
                              <Col md={12} sm={4} xs={4} className="mb-2">
                                {" "}
                                <div>
                                  <b>Order Id : </b>#{order.order_id}
                                </div>
                              </Col>
                              <Col md={12} sm={4} xs={4} className="mb-2">
                                <div>
                                  <b>Order Placed Date : </b>
                                  {moment(order.order_created_at).format(
                                    "DD MMM YYYY"
                                  )}
                                </div>
                              </Col>
                              <Col md={12} sm={4} xs={4} className="mb-2">
                                <div>
                                  <b>Amount : </b>&#x20b9;
                                  {order.order_total.grand_total.toFixed(2)}
                                </div>
                              </Col>
                            </Row>
                          </Col>
                          <Col
                            md={4}
                            sm={12}
                            xs={12}
                            className="order-2 order-md-2 order-sm-1 order-xs-1 order-lg-2"
                          >
                            {order.order_car_details.vehicle_image && (
                              <img
                                alt={order.order_car_details.vehicle_name}
                                src={order.order_car_details.vehicle_image}
                                style={{ width: "100%" }}
                              />
                            )}
                            <h3 className="text-center mt-2">
                              {order.order_car_details.brand_name}
                              {order.order_car_details.vehicle_name}
                            </h3>
                          </Col>
                          <Col
                            className="p-0 text-align-set order-3 order-md-3 order-sm-3 order-xs-3 order-lg-3"
                            md={4}
                            sm={12}
                            xs={12}
                          >
                            <Row>
                              <Col md={12} sm={6} xs={6} className="mb-2">
                                {" "}
                                <div>
                                  <b>Type : </b>
                                  <GetOrderStatus order={order} />
                                </div>
                              </Col>
                              <Col md={12} sm={6} xs={6} className="mb-2">
                                <div>
                                  <b>Date : </b>
                                  {moment(order.order_location.date).format(
                                    "DD MMM YYYY"
                                  )}
                                </div>
                              </Col>
                              <Col md={12} sm={6} xs={6} className="mb-2">
                                <div>
                                  <b>Time : </b>
                                  {`${convertTime(
                                    order.order_location.start_time
                                  )} - ${convertTime(
                                    order.order_location.end_time
                                  )}`}
                                </div>
                              </Col>
                            </Row>
                          </Col>
                        </Row>
                        <Row className="text-right text-align-set-two">
                          <Col className="">
                            <div>
                              <Button
                                variant="contained"
                                color="primary"
                                className="botton-style-orderdetails"
                                block={true}
                                onClick={() => {
                                  this.handleImage();
                                }}
                              >
                                View Images
                              </Button>
                            </div>
                          </Col>
                        </Row>
                      </CardBody>
                    </Card>
                  </Col>
                  <Col>
                    <div className="jr-card p-3">
                      <div className="jr-card-header mb-3 d-flex">
                        <h3 className="mb-0 mr-auto">Estimation</h3>
                        {this.state.isEditEstimation && (
                          <Button
                            color={"primary"}
                            variant="contained"
                            size="small"
                            onClick={() => {
                              this.updateEstimation();
                            }}
                            className="mr-2"
                          >
                            <Save className="mr-2" fontSize="inherit" /> Update
                            & Send
                          </Button>
                        )}
                        {this.state.isAllowedEdit && (
                          <Button
                            color={
                              this.state.isEditEstimation
                                ? "secondary"
                                : "primary"
                            }
                            variant="contained"
                            size="small"
                            onClick={() => {
                              this.handleCliclEdit();
                            }}
                          >
                            {this.state.isEditEstimation ? (
                              <React.Fragment>
                                <Cancel fontSize="inherit" className="mr-2" />{" "}
                                Cancel
                              </React.Fragment>
                            ) : (
                              <React.Fragment>
                                <Edit fontSize="inherit" className="mr-2" />{" "}
                                Edit{" "}
                              </React.Fragment>
                            )}
                          </Button>
                        )}
                      </div>
                      <div className="table-responsive-material">
                        <table className="project-list-table table remove-table-border mb-0">
                          <thead>
                            <tr>
                              <th scope="col">Name</th>
                              <th scope="col" className="text-right">
                                Price
                              </th>
                            </tr>
                          </thead>
                          <tbody>
                            {order.order_items.map((item, index) => {
                              return (
                                <tr tabIndex={-1} key={"projects-" + index}>
                                  <td className="max-width-100 ">
                                    <p className="text-truncate mb-0">
                                      <Badge color="dark">{index + 1}</Badge>
                                      <Tooltip title={item.name}>
                                        <span>
                                          <Button
                                            disabled
                                            className="text-dark"
                                          >
                                            {" "}
                                            {item.name}
                                          </Button>
                                        </span>
                                      </Tooltip>
                                      {item.description !== null ? (
                                        <Tooltip title="What's Include">
                                          <IconButton
                                            size="small"
                                            onClick={() => {
                                              this.handleInclude(item);
                                            }}
                                          >
                                            <InfoIcon
                                              color="primary"
                                              fontSize="inherit"
                                            />
                                          </IconButton>
                                        </Tooltip>
                                      ) : null}
                                    </p>
                                  </td>
                                  <td className="text-nowrap text-right">
                                    &#x20b9;{item.price.toFixed(2)}
                                    {this.state.isEditEstimation && (
                                      <IconButton
                                        onClick={() => {
                                          this.removeItem(index);
                                        }}
                                        size="small"
                                      >
                                        <Delete
                                          color="error"
                                          fontSize="inherit"
                                        />
                                      </IconButton>
                                    )}
                                  </td>
                                </tr>
                              );
                            })}
                            {this.state.isEditEstimation && (
                              <tr tabIndex={-1} key={"projects-item-add"}>
                                <td
                                  colspan="2"
                                  className="text-nowrap text-right font-weight-bold"
                                >
                                  <Button
                                    onClick={() => {
                                      this.onAddClick();
                                    }}
                                    size="small"
                                    variant="contained"
                                    color={"default"}
                                  >
                                    <Add fontSize="inherit" className="mr-2" />{" "}
                                    New Item
                                  </Button>
                                </td>
                              </tr>
                            )}
                            <tr tabIndex={-1} key={"projects-item-total"}>
                              <td className="max-width-100">
                                <p className="text-truncate mb-0 font-weight-bold">
                                  Item Total
                                </p>
                              </td>
                              <td className="text-nowrap text-right font-weight-bold">
                                &#x20b9;
                                {order.order_items
                                  .reduce(
                                    (total, item) => total + item.price,
                                    0
                                  )
                                  .toFixed(2)}
                              </td>
                            </tr>
                            <tr tabIndex={-1} key={"projects-discount"}>
                              <td className="max-width-100">
                                <p className="text-truncate mb-0 font-weight-bold">
                                  Discount
                                </p>
                              </td>
                              <td className="text-nowrap text-right font-weight-bold">
                                -&#x20b9;
                                {Number(
                                  order.order_discount.discounted_value || 0
                                ).toFixed(2)}
                              </td>
                            </tr>
                            <tr tabIndex={-1} key={"projects-total"}>
                              <td className="max-width-100">
                                <p className="text-truncate mb-0 font-weight-bold">
                                  Total
                                </p>
                              </td>
                              <td className="text-nowrap text-right font-weight-bold">
                                &#x20b9;
                                {(
                                  order.order_items.reduce(
                                    (total, item) => total + item.price,
                                    0
                                  ) -
                                  Number(
                                    order.order_discount.discounted_value || 0
                                  )
                                ).toFixed(2)}
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </Col>
                </Row>
              </Col>
              <Col md={3}>
                <UserProfileCard
                  user={
                    order.order_door_step === 0
                      ? order.order_garage
                      : order.order_mobile_van
                  }
                  address={
                    order.order_door_step === 0
                      ? order.order_garage
                      : order.order_mobile_van
                  }
                  userType={
                    order.order_door_step === 0 ? "GARAGE" : "MOBILEVAN"
                  }
                  garageImage={order.garage_profile_image}
                />
                {order.order_door_step === 0 ? (
                  order.order_garage !== null ? (
                    <Chat
                      roomNumber={`order_${order.order_id}_advisor_garage`}
                      order={order}
                      currentUser={order.order_advisor}
                      toUser={order.order_garage}
                      role="garage"
                      advisorprofileimage={order.advisor_profile_image}
                      garageprofileimage={order.garage_profile_image}
                    />
                  ) : null
                ) : null}
              </Col>
            </Row>
            <Dialog
              onClose={this.handleRequestCloseOptionModel}
              open={this.state.isSelectOptionModel}
              fullWidth
            >
              <DialogTitle className="p-2">
                <div className="d-flex">
                  <div className="ml-auto">
                    <Tooltip title={"Close"}>
                      <IconButton
                        color="primary"
                        className="p-0"
                        onClick={() => this.handleRequestCloseOptionModel()}
                      >
                        <CloseIcon />
                      </IconButton>
                    </Tooltip>
                  </div>
                </div>
              </DialogTitle>
              <DialogContent>
                <Row>
                  <Col md={12}>
                    <ButtonGroup fullWidth>
                      <Button
                        variant={
                          this.state.selectOptionModelType === "SERVICE"
                            ? "contained"
                            : "contained"
                        }
                        onClick={() => {
                          this.handleOptionBtn("SERVICE");
                        }}
                        color={
                          this.state.selectOptionModelType === "SERVICE"
                            ? "primary"
                            : "default"
                        }
                      >
                        System Services
                      </Button>
                      <Button
                        variant={
                          this.state.selectOptionModelType === "PACKAGE"
                            ? "contained"
                            : "contained"
                        }
                        onClick={() => {
                          this.handleOptionBtn("PACKAGE");
                        }}
                        color={
                          this.state.selectOptionModelType === "PACKAGE"
                            ? "primary"
                            : "default"
                        }
                      >
                        System Packages
                      </Button>
                      <Button
                        variant={
                          this.state.selectOptionModelType === "CUSTOME"
                            ? "contained"
                            : "contained"
                        }
                        onClick={() => {
                          this.handleOptionBtn("CUSTOME");
                        }}
                        color={
                          this.state.selectOptionModelType === "CUSTOME"
                            ? "primary"
                            : "default"
                        }
                      >
                        Custome
                      </Button>
                    </ButtonGroup>
                  </Col>
                  <Col md={12}>
                    <Collapse
                      isOpen={this.state.selectOptionModelType === "SERVICE"}
                    >
                      <List>
                        <ListItem disableGutters>
                          <FormControl
                            variant="outlined"
                            size="small"
                            fullWidth
                          >
                            <InputLabel htmlFor="outlined-adornment-password">
                              Search
                            </InputLabel>
                            <OutlinedInput
                              id="outlined-adornment-password"
                              type={"text"}
                              value={this.state.txtSearch}
                              onChange={(event) => {
                                this.setState({
                                  txtSearch: event.target.value,
                                });
                              }}
                              onKeyDown={(event) => {
                                event.key === "Enter" && this.onSearchClick();
                              }}
                              endAdornment={
                                <InputAdornment position="end">
                                  <IconButton
                                    onClick={() => {
                                      this.onSearchClick();
                                    }}
                                  >
                                    <Search />
                                  </IconButton>
                                </InputAdornment>
                              }
                              labelWidth={50}
                            />
                          </FormControl>
                        </ListItem>
                        {this.state.listServices.length === 0 && (
                          <ListItem disableGutters>
                            <ListItemText>
                              <div className="text-center text-muted">
                                No records found!
                              </div>
                            </ListItemText>
                          </ListItem>
                        )}
                        {this.state.listServices.map((item) => (
                          <ListItem disableGutters key={item.service_id}>
                            <ListItemIcon>
                              <Avatar src={item.imageurls[0]} />
                            </ListItemIcon>
                            <ListItemText>
                              <Row>
                                <Col className="m-auto">
                                  <div className="font-weight-bold text-capitalize">
                                    {`${item.service_name}`}
                                    {item.service_description !== null ? (
                                      <Tooltip title="What's Include">
                                        <IconButton
                                          size="small"
                                          onClick={() => {
                                            this.handleInclude(item);
                                          }}
                                        >
                                          <InfoIcon
                                            color="primary"
                                            fontSize="inherit"
                                          />
                                        </IconButton>
                                      </Tooltip>
                                    ) : null}
                                  </div>
                                  {/* <div className="font-weight-normal text-sm text-truncate">
                                    {item.service_description
                                      .split("\n")
                                      .join(" | ")}
                                  </div> */}
                                </Col>
                                <Col className="text-right">
                                  <div className="d-inline">
                                    {item.service_discount.type !== "" && (
                                      <div
                                        className="text-success text-muted d-inline"
                                        style={{ fontSize: "small" }}
                                      >
                                        <s>
                                          &#x20b9;
                                          {item.service_price.toFixed(2)}
                                        </s>
                                      </div>
                                    )}
                                    <div className="text-success d-inline ml-2">
                                      &#x20b9;
                                      {this.getPrice(
                                        item.service_price,
                                        item.service_discount
                                      ).toFixed(2)}
                                    </div>
                                  </div>
                                  <div className="d-block">
                                    {this.isItemAdded(item.service_id, 0) ? (
                                      <Button
                                        color={"secondary"}
                                        variant="outlined"
                                        size="small"
                                        disabled
                                      >
                                        Added
                                      </Button>
                                    ) : (
                                      <Button
                                        color={"primary"}
                                        variant="contained"
                                        size="small"
                                        onClick={() =>
                                          this.handleListItemClick(
                                            item.service_id,
                                            item.service_name,
                                            this.getPrice(
                                              item.service_price,
                                              item.service_discount
                                            ).toFixed(2),
                                            item.service_price,
                                            0,
                                            item.service_description
                                          )
                                        }
                                      >
                                        Add
                                      </Button>
                                    )}
                                  </div>
                                </Col>
                              </Row>
                            </ListItemText>
                          </ListItem>
                        ))}
                      </List>
                    </Collapse>
                  </Col>
                  <Col md={12}>
                    <Collapse
                      isOpen={this.state.selectOptionModelType === "PACKAGE"}
                    >
                      <List>
                        {this.state.listPackages.length === 0 && (
                          <ListItem disableGutters>
                            <ListItemText>
                              <div className="text-center text-muted">
                                No records found!
                              </div>
                            </ListItemText>
                          </ListItem>
                        )}
                        {this.state.listPackages.map((item) => (
                          <ListItem
                            disableGutters
                            key={item.service_package_id}
                          >
                            <ListItemIcon>
                              <Avatar src={item.imageurls[0]} />
                            </ListItemIcon>
                            <ListItemText>
                              <Row>
                                <Col className="m-auto">
                                  <div className="font-weight-bold text-capitalize">
                                    {`${item.package_name}`}
                                    {item.service_description !== null ? (
                                      <Tooltip title="What's Include">
                                        <IconButton
                                          size="small"
                                          onClick={() => {
                                            this.handleInclude(item);
                                          }}
                                        >
                                          <InfoIcon
                                            color="primary"
                                            fontSize="inherit"
                                          />
                                        </IconButton>
                                      </Tooltip>
                                    ) : null}
                                  </div>
                                  {/* <div className="font-weight-normal text-sm text-truncate">
                                    {item.package_description
                                      .split("\n")
                                      .join(" | ")}
                                  </div> */}
                                </Col>
                                <Col className="text-right">
                                  <div className="d-inline">
                                    {item.package_discount.type !== "" && (
                                      <div
                                        className="text-success text-muted d-inline"
                                        style={{ fontSize: "small" }}
                                      >
                                        <s>
                                          &#x20b9;
                                          {item.package_price.toFixed(2)}
                                        </s>
                                      </div>
                                    )}
                                    <div className="text-success d-inline ml-2">
                                      &#x20b9;
                                      {this.getPrice(
                                        item.package_price,
                                        item.package_discount
                                      ).toFixed(2)}
                                    </div>
                                  </div>
                                  <div className="d-block">
                                    {this.isItemAdded(
                                      item.service_package_id,
                                      1
                                    ) ? (
                                      <Button
                                        color={"secondary"}
                                        variant="outlined"
                                        size="small"
                                        disabled
                                      >
                                        Added
                                      </Button>
                                    ) : (
                                      <Button
                                        color={"primary"}
                                        variant="contained"
                                        size="small"
                                        onClick={() =>
                                          this.handleListItemClick(
                                            item.service_package_id,
                                            item.package_name,
                                            this.getPrice(
                                              item.package_price,
                                              item.package_discount
                                            ).toFixed(2),
                                            item.package_price,
                                            1,
                                            item.package_description
                                          )
                                        }
                                      >
                                        Add
                                      </Button>
                                    )}
                                  </div>
                                </Col>
                              </Row>
                            </ListItemText>
                          </ListItem>
                        ))}
                      </List>
                    </Collapse>
                  </Col>
                  <Col md={12}>
                    <Collapse
                      isOpen={this.state.selectOptionModelType === "CUSTOME"}
                    >
                      <Row md={1} sm={1} lg={1} className="mt-3">
                        <Col>
                          <FormControl
                            variant="outlined"
                            size="small"
                            fullWidth
                            className="mb-2"
                          >
                            <InputLabel htmlFor="outlined-adornment-password">
                              Service Info
                            </InputLabel>
                            <OutlinedInput
                              id="outlined-adornment-password"
                              type={"text"}
                              value={this.state.customeService.info}
                              onChange={(event) => {
                                let cs = this.state.customeService;
                                cs.info = event.target.value;
                                this.setState({ customeService: cs });
                              }}
                              multiline
                              rows={3}
                              labelWidth={90}
                            />
                          </FormControl>
                        </Col>
                        <Col>
                          <FormControl
                            variant="outlined"
                            size="small"
                            fullWidth
                            className="mb-2"
                          >
                            <InputLabel htmlFor="outlined-adornment-password">
                              Service Price
                            </InputLabel>
                            <OutlinedInput
                              id="outlined-adornment-password"
                              type={"number"}
                              onInput={(e) => {
                                e.target.value = Math.max(
                                  0,
                                  parseInt(e.target.value)
                                ).toString();
                              }}
                              min={0}
                              value={this.state.customeService.price}
                              onChange={(event) => {
                                let cs = this.state.customeService;
                                cs.price = event.target.value;
                                this.setState({ customeService: cs });
                              }}
                              labelWidth={90}
                            />
                          </FormControl>
                        </Col>
                        <Col>
                          <Button
                            disabled={
                              this.state.customeService.info === "" ||
                              this.state.customeService.price === ""
                            }
                            color="primary"
                            variant="contained"
                            size="small"
                            fullWidth
                            onClick={() => {
                              this.handleListItemClick(
                                null,
                                this.state.customeService.info,
                                this.state.customeService.price,
                                0,
                                0,
                                ""
                              );
                              this.handleRequestCloseOptionModel();
                            }}
                          >
                            Add
                          </Button>
                        </Col>
                      </Row>
                    </Collapse>
                  </Col>
                </Row>
              </DialogContent>
            </Dialog>
          </DialogContent>
        </Dialog>
        {this.state.isImagedetails && (
          <IamageDilog
            isVisible={this.state.isImagedetails}
            isLoading={this.state.isModelLoading}
            imagedata={this.state.imagedata}
            onSubmit={(isConfirm, data) => {
              this.handleImageConform(isConfirm, data);
            }}
          />
        )}
        {this.state.isInclude && (
          <WhatsInclude
            isVisible={this.state.isInclude}
            isLoading={this.state.isModelLoading}
            data={this.state.includesdata}
            onSubmit={() => {
              this.handleIncludeClose();
            }}
          />
        )}
      </div>
    );
  }
}

export default ModelOrderDetail;
