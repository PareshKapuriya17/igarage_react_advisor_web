import React from "react";
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  Button,
  CircularProgress,
  // DialogActions,
} from "@material-ui/core";
import "./order.scss";
import { GetApp } from "@material-ui/icons";
import {
  // Container,
  Row,
  Col,

  //   Label,
  //   CardText,
} from "reactstrap";
import { AppSettings } from "constants/AppSettings";
import { Tooltip, IconButton } from "@material-ui/core";
import pdf from "assets/images/pdf.png";
import CloseIcon from "@material-ui/icons/CancelOutlined";
export default class ConfirmDialog extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: this.props.isVisible || false,
      imagedata: this.props.imagedata,
    };
  }

  handleRequestClose = (is) => {
    this.props.onSubmit(is);
  };

  render() {
    debugger;
    const order =
      this.state.imagedata !== null
        ? this.state.imagedata.pickup !== undefined
          ? this.state.imagedata.pickup
          : this.state.imagedata.dropoff
        : null;

    // const delivar = this.state.imagedata.delivery;
    return (
      <Dialog
        disableBackdropClick
        open={this.state.open}
        onClose={() => this.handleRequestClose(false)}
        fullWidth={"md"}
      >
        {this.state.isLoading && (
          <div className="model-loader-view">
            <CircularProgress />
          </div>
        )}
        <DialogTitle>
          <div className="d-flex">
            <h3> View Image</h3>
            <div className="ml-auto">
              <Tooltip title={"Close"}>
                <IconButton
                  color="primary"
                  className="p-0"
                  onClick={() => this.handleRequestClose(false)}
                >
                  <CloseIcon />
                </IconButton>
              </Tooltip>
            </div>
          </div>
        </DialogTitle>
        <DialogContent>
          <DialogContentText>
            <Row>
              <Col md={12}>
                <h3 className="image-dilog mb-10">
                  {/* {this.state.imagedata.pickup !== undefined
                    ? "Pickup"
                    : "Self Drop"} */}
                  Pickup
                </h3>
                <div>
                  {this.state.imagedata !== null ? (
                    order !== undefined ? (
                      order.order_images !== null ? (
                        <Row>
                          {order.order_images.map((item, index) => {
                            debugger;
                            var extention = item.split(".").pop();
                            switch (extention.toLowerCase()) {
                              case "jpg":
                              case "png":
                              case "svg":
                                return (
                                  <Col
                                    md={3}
                                    xs={3}
                                    sm={3}
                                    className="mb-10 m-mb-20 text-center"
                                    key={index}
                                  >
                                    <img
                                      src={`${AppSettings.SERVER_IMAGE_URL}${item}`}
                                      className="img-fluid image-style-dilog"
                                      width="120"
                                      height="120"
                                      alt="cardetails"
                                    />
                                    <div className="mt-3 mb-3 text-center">
                                      <a
                                        href={`${AppSettings.API_BASE_URL}download?file=${item}`}
                                        download
                                        rel="noopener noreferrer"
                                      >
                                        <Button
                                          color="primary"
                                          variant="outlined"
                                          // onClick={() => {
                                          //   this.removeFile(index);
                                          // }}
                                        >
                                          <Tooltip title={"Download"}>
                                            <GetApp />
                                          </Tooltip>
                                        </Button>
                                      </a>
                                      {/* <Button
                                        color="primary"
                                        variant="outlined"
                                        onClick={() => {
                                          this.removeFile(index);
                                        }}
                                      >
                                        <Tooltip title={"Delete"}>
                                          <Delete color="error" />
                                        </Tooltip>
                                      </Button> */}
                                    </div>
                                  </Col>
                                );
                              case "pdf":
                                return (
                                  <Col
                                    md={3}
                                    xs={3}
                                    sm={3}
                                    className="mb-10 m-mb-20 text-center"
                                    key={index}
                                  >
                                    <img
                                      src={pdf}
                                      className="img-fluid image-style-dilog"
                                      width="120"
                                      height="120"
                                      alt="cardetails"
                                    />
                                    <div className="mt-3 mb-3 text-center">
                                      <a
                                        href={`${AppSettings.API_BASE_URL}download?file=${item}`}
                                        download
                                        rel="noopener noreferrer"
                                      >
                                        <Button
                                          color="primary"
                                          variant="outlined"
                                          // onClick={() => {
                                          //   this.removeFile(index);
                                          // }}
                                        >
                                          <Tooltip title={"Download"}>
                                            <GetApp />
                                          </Tooltip>
                                        </Button>
                                      </a>
                                      {/* <Button
                                        color="primary"
                                        variant="outlined"
                                        onClick={() => {
                                          this.removeFile(index);
                                        }}
                                      >
                                        <Tooltip title={"Delete"}>
                                          <Delete color="error" />
                                        </Tooltip>
                                      </Button> */}
                                    </div>
                                  </Col>
                                );
                              default:
                                return (
                                  <div className="text-center ">
                                    <h1>Currept Document</h1>
                                  </div>
                                );
                            }
                          })}
                        </Row>
                      ) : (
                        <div>
                          <p>No Image Available</p>
                        </div>
                      )
                    ) : (
                      <div>
                        <p>No Image Available</p>
                      </div>
                    )
                  ) : (
                    <div>
                      <p>No Image Available</p>
                    </div>
                  )}
                </div>
              </Col>
            </Row>
            <Row className="mt-20">
              <Col md={12}>
                <h3 className="image-dilog mb-10">Delivery</h3>
                <div>
                  {this.state.imagedata !== null ? (
                    this.state.imagedata.delivery !== undefined ? (
                      this.state.imagedata.delivery.order_images !== null ? (
                        <Row>
                          {this.state.imagedata.delivery.order_images.map(
                            (item, index) => {
                              debugger;
                              var extention = item.split(".").pop();
                              switch (extention.toLowerCase()) {
                                case "jpg":
                                case "png":
                                case "svg":
                                  return (
                                    <Col
                                      md={3}
                                      xs={3}
                                      sm={3}
                                      className="mb-10 m-mb-20 text-center"
                                      key={index}
                                    >
                                      <img
                                        src={`${AppSettings.SERVER_IMAGE_URL}${item}`}
                                        className="img-fluid image-style-dilog"
                                        width="120"
                                        height="120"
                                        alt="cardetails"
                                      />
                                      <div className="mt-3 mb-3 text-center">
                                        <a
                                          href={`${AppSettings.API_BASE_URL}download?file=${item}`}
                                          download
                                          rel="noopener noreferrer"
                                        >
                                          <Button
                                            color="primary"
                                            variant="outlined"
                                            // onClick={() => {
                                            //   this.removeFile(index);
                                            // }}
                                          >
                                            <Tooltip title={"Download"}>
                                              <GetApp />
                                            </Tooltip>
                                          </Button>
                                        </a>
                                        {/* <Button
                                        color="primary"
                                        variant="outlined"
                                        onClick={() => {
                                          this.removeFile(index);
                                        }}
                                      >
                                        <Tooltip title={"Delete"}>
                                          <Delete color="error" />
                                        </Tooltip>
                                      </Button> */}
                                      </div>
                                    </Col>
                                  );
                                case "pdf":
                                  return (
                                    <Col
                                      md={3}
                                      xs={3}
                                      sm={3}
                                      className="mb-10 m-mb-20 text-center"
                                      key={index}
                                    >
                                      <img
                                        src={pdf}
                                        className="img-fluid image-style-dilog"
                                        width="120"
                                        height="120"
                                        alt="cardetails"
                                      />
                                      <div className="mt-3 mb-3 text-center">
                                        <a
                                          href={`${AppSettings.API_BASE_URL}download?file=${item}`}
                                          download
                                          rel="noopener noreferrer"
                                        >
                                          <Button
                                            color="primary"
                                            variant="outlined"
                                            // onClick={() => {
                                            //   this.removeFile(index);
                                            // }}
                                          >
                                            <Tooltip title={"Download"}>
                                              <GetApp />
                                            </Tooltip>
                                          </Button>
                                        </a>
                                        {/* <Button
                                        color="primary"
                                        variant="outlined"
                                        onClick={() => {
                                          this.removeFile(index);
                                        }}
                                      >
                                        <Tooltip title={"Delete"}>
                                          <Delete color="error" />
                                        </Tooltip>
                                      </Button> */}
                                      </div>
                                    </Col>
                                  );
                                default:
                                  return (
                                    <div className="text-center ">
                                      <h1>Currept Document</h1>
                                    </div>
                                  );
                              }
                            }
                          )}
                        </Row>
                      ) : (
                        <div>
                          <p>No Image Available</p>
                        </div>
                      )
                    ) : (
                      <div>
                        <p>No Image Available</p>
                      </div>
                    )
                  ) : (
                    <div>
                      <p>No Image Available</p>
                    </div>
                  )}
                </div>
              </Col>
            </Row>
          </DialogContentText>
        </DialogContent>
      </Dialog>
    );
  }
}
