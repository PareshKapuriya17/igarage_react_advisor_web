import React, { useState } from "react";
import Avatar from "@material-ui/core/Avatar";
import { useDispatch, useSelector } from "react-redux";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import { userSignOut } from "actions/Auth";
// import { getUser } from "actions/Auth";
import IntlMessages from "util/IntlMessages";
// import Routes from "app/routes/index";
import { useHistory } from "react-router-dom";

const UserInfo = (props) => {
  const dispatch = useDispatch();
  debugger;
  const { authUser } = useSelector((state) => state.auth);

  const [anchorE1, setAnchorE1] = useState(null);
  const [open, setOpen] = useState(false);
  const history = useHistory();

  // const routeResult = useRoutes(Routes)

  const handleClick = (event) => {
    setOpen(true);
    setAnchorE1(event.currentTarget);
  };

  const handleProfileClick = () => {
    debugger;
    history.push("/app/profile");
    // console.log(history);
  };
  const handlePasswordChangeClick = () => {
    debugger;
    history.push("/app/change-password");
    // console.log(history);
  };

  const handleRequestClose = () => {
    setOpen(false);
  };

  const userImage = authUser
    ? authUser.user_profile_image !== ""
      ? authUser.user_profile_image
      : authUser.user_profile_image
    : "https://via.placeholder.com/150x150";
  return (
    <div className="user-profile d-flex flex-row align-items-center">
      <Avatar
        alt="..."
        src={userImage}
        className="user-avatar "
        onClick={handleClick}
      />
      <div className="user-detail d-none d-sm-block">
        <h4 className="user-name text-white" onClick={handleClick}>
          {authUser ? `${authUser.user_name}` : ""}
          <i className="zmdi zmdi-caret-down zmdi-hc-fw align-middle" />
        </h4>
      </div>
      <Menu
        className="user-info"
        id="simple-menu"
        anchorEl={anchorE1}
        open={open}
        onClose={handleRequestClose}
        PaperProps={{
          style: {
            minWidth: 120,
            paddingTop: 0,
            paddingBottom: 0,
          },
        }}
      >
        <MenuItem
          onClick={() => {
            handleProfileClick();
            handleRequestClose();
          }}
        >
          <i className="zmdi zmdi-account zmdi-hc-fw mr-2" />
          <IntlMessages id="popup.profile" />
        </MenuItem>
        <MenuItem
          onClick={() => {
            handleRequestClose();
            handlePasswordChangeClick();
          }}
        >
          <i className="zmdi zmdi-settings zmdi-hc-fw mr-2" />
          {/* <IntlMessages id="popup.setting"/> */}
          Change Password
        </MenuItem>
        <MenuItem
          onClick={() => {
            handleRequestClose();
            dispatch(userSignOut());
          }}
        >
          <i className="zmdi zmdi-sign-in zmdi-hc-fw mr-2" />

          <IntlMessages id="popup.logout" />
        </MenuItem>
      </Menu>
    </div>
  );
};

export default UserInfo;
