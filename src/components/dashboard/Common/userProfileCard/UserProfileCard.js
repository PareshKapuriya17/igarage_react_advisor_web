import React from "react";
import { Avatar } from "@material-ui/core";

const UserProfileCard = ({
  user,
  address,
  userType,
  customerimage,
  garageImage,
}) => {
  debugger;
  if (user === null) {
    return <div></div>;
  }
  switch (userType) {
    case "CUSTOMER":
      return (
        <div className="jr-card text-center">
          <div className={`jr-card-header-color bg-dark`}>
            <Avatar
              className={"rounded-circle size-90 avatar-shadow mb-3 mx-auto"}
              src={customerimage}
            />
            <div className="jr-card-hd-content">
              <h5 className="mb-0 text-white">{user.user_name}</h5>
              <p className="jr-fs-sm mb-0 text-grey text-lighten-2">
                {user.user_email}
              </p>
              <p className="jr-fs-sm mb-0 text-grey text-lighten-2">
                {user.user_mobile}
              </p>
            </div>
            {/* <Fab className="jr-badge-up bg-success"><i className="zmdi zmdi-mail-send"/></Fab> */}
          </div>
          <div class="jr-card-body pt-2">
            <p class="card-text">
              {address.address_line}, {address.city_name} -{" "}
              {address.address_pincode}
            </p>
          </div>
        </div>
      );
    case "GARAGE":
      return (
        <div className="jr-card text-center">
          <div className={`jr-card-header-color bg-dark`}>
            <Avatar
              className={"rounded-circle size-90 avatar-shadow mb-3 mx-auto"}
              src={garageImage}
            />
            <div className="jr-card-hd-content">
              <h5 className="mb-0 text-white">{user.garage_name}</h5>
              <p className="jr-fs-sm mb-0 text-grey text-lighten-2">
                {user.user_email}
              </p>
              <p className="jr-fs-sm mb-0 text-grey text-lighten-2">
                {user.user_mobile}
              </p>
            </div>
            {/* <Fab className="jr-badge-up bg-success"><i className="zmdi zmdi-mail-send"/></Fab> */}
          </div>
          <div class="jr-card-body pt-2">
            <p class="card-text">
              {address.garage_address}, {address.city_name} -{" "}
              {address.garage_pincode}
            </p>
          </div>
        </div>
      );
    case "MOBILEVAN":
      return (
        <div className="jr-card text-center">
          <div className={`jr-card-header-color bg-dark`}>
            <Avatar
              className={"rounded-circle size-90 avatar-shadow mb-3 mx-auto"}
              src={user.user_profile_image}
            />
            <div className="jr-card-hd-content">
              <h5 className="mb-0 text-white">{user.mobile_van_name}</h5>
              <p className="jr-fs-sm mb-0 text-grey text-lighten-2">
                {user.mobile_van_person_name}
              </p>
              <p className="jr-fs-sm mb-0 text-grey text-lighten-2">
                {user.mobile_van_person_mobile}
              </p>
              <p className="jr-fs-sm mb-0 text-grey text-lighten-2">
                {user.mobile_van_number}
              </p>
            </div>
            {/* <Fab className="jr-badge-up bg-success"><i className="zmdi zmdi-mail-send"/></Fab> */}
          </div>
          <div class="jr-card-body pt-2">
            <p class="card-text">{user.mobile_van_details}</p>
          </div>
        </div>
      );

    default:
      break;
  }
};

export default UserProfileCard;
