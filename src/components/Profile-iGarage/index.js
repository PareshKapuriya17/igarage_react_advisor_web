import React from "react";
// import { getUser } from 'actions/Auth';
import ApiRequests from "util/Requests";
import { Row, Col, Container } from "reactstrap";
import {
  Button,
  TextField,
  IconButton,
  CircularProgress,
} from "@material-ui/core";
import PhotoCamera from "@material-ui/icons/PhotoCamera";
import "styles/profile.scss";
import { Formik } from "formik";
import * as Yup from "yup";
import {
  NotificationContainer,
  NotificationManager,
} from "react-notifications";
// import { useHistory } from "react-router-dom";

export class index extends React.Component {
  constructor(props) {
    super(props);
    this.fileInputRef = React.createRef();
    this.state = {
      isLoading: false,
      data: {
        user_profile_image: "",
        user_name: "sss",
        user_mobile: "",
        user_email: "",
        remove_current_image: 0,
      },
      isEditable: false,
      imageUrl: "",
    };
    this.getProfileData();
  }
  handleRequestClose = () => {
    this.props.history.push("/app/dashboard");
  };
  getProfileData = () => {
    debugger;
    ApiRequests.getUserProfile()
      .then((res) => {
        debugger;
        if (res.data.status === 1) {
          this.setState({
            data: res.data.data,
            imageUrl: res.data.data.user_profile_image,
            isLoading: false,
          });
        }
      })
      .catch((err) => {
        NotificationManager.error(err.response.data.message);
        this.setState({ isLoading: false });
      });
  };
  filehandler = (event) => {
    console.log(event.target.files[0]);
    let data = this.state.data;
    data.user_profile_image = event.target.files[0];
    data.remove_current_image = 1;
    this.setState({
      data: data,
      imageUrl: URL.createObjectURL(event.target.files[0]),
    });
  };
  setEditable = () => {
    this.setState({
      isEditable: true,
    });
  };
  handleClickSubmit = (values) => {
    debugger;

    let formData = new FormData();
    formData.append("user_email", values.user_email);
    formData.append("user_name", values.user_name);
    formData.append("user_mobile", values.user_mobile);
    formData.append("user_profile_image", this.state.data.user_profile_image);
    formData.append(
      "remove_current_image",
      this.state.data.remove_current_image
        ? this.state.data.remove_current_image
        : 0
    );
    this.setState({ isLoading: true });

    ApiRequests.updateProfile(formData)
      .then((res) => {
        debugger;
        if (res.data.status === 1) {
          NotificationManager.success("Service Advisor updated!");
          this.setState({ data: res.data.data, isLoading: false });
          this.getProfileData();
        }
      })
      .catch((err) => {
        NotificationManager.error(err.response.data.message);
        this.setState({ pagination: { total: 0, page: 0, perPage: 10 } });
      });
  };
  handleClick(e) {
    debugger;
    // this.fileInputRef.fileUploader.click();
    this.fileInputRef.current.click();
  }
  handleNameChange = (e) => {
    let data = this.state.data;
    data.user_Name = e.value;
    this.setState({
      data: data,
    });
  };

  render() {
    return (
      <div className="profile-intro card shadow  border-0 text-center m-5">
        <div className="pi-header">
          <div className="card-image">
            <img
              className="avatar-circle"
              src={this.state.imageUrl}
              alt="Team Member"
            />

            <div>
              <input
                accept="image/*"
                className="file-input-disable-none"
                id="contained-button-file"
                onChange={(event) => this.filehandler(event)}
                ref={this.fileInputRef}
                type="file"
              />

              <label htmlFor="icon-button-file">
                <IconButton
                  className="upload-button"
                  color="primary"
                  aria-label="upload picture"
                  component="span"
                  onClick={(event) => this.handleClick(event)}
                >
                  <PhotoCamera />
                </IconButton>
              </label>
            </div>
          </div>
        </div>
        <div className="mt-3">
          <h3>{this.state.data.user_name}</h3>
          <p>{this.state.data.user_email}</p>
        </div>
        <div className="pi-footer">
          <Formik
            enableReinitialize
            initialValues={{ ...this.state.data }}
            onSubmit={(event) => this.handleClickSubmit(event)}
            validationSchema={Yup.object().shape({
              user_name: Yup.string().required("Please enter name"),
              user_email: Yup.string().required("Required"),
              user_mobile: Yup.string()
                .matches(/^[0-9]+$/, "Must be only digits")
                .max(10, "Must be 10 Digit")
                .min(10, "Must be 10 Digit")
                .required("Please enter mobile no."),
            })}
          >
            {(props) => {
              const {
                values,
                touched,
                errors,
                // dirty,
                handleChange,
                handleBlur,
                handleSubmit,
                // setFieldValue,
                // handleReset,
              } = props;
              return (
                <form onSubmit={handleSubmit}>
                  <Container>
                    <Col lg={6} className="margin-zero-auto">
                      <Row className="justify-content-md-center">
                        <Col lg={12}>
                          <TextField
                            value={values.user_name}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            name="user_name"
                            margin="dense"
                            label="Name"
                            type="text"
                            fullWidth
                            required
                            variant="outlined"
                            error={errors.user_name && touched.user_name}
                            helperText={
                              errors.user_name &&
                              touched.user_name &&
                              errors.user_name
                            }
                          />
                        </Col>

                        <Col lg="12">
                          <TextField
                            value={values.user_email}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            name="user_email"
                            margin="dense"
                            label="Email"
                            type="text"
                            fullWidth
                            required
                            variant="outlined"
                            disabled
                            error={errors.user_email && touched.user_email}
                            helperText={
                              errors.user_email &&
                              touched.user_email &&
                              errors.user_email
                            }
                          />
                        </Col>

                        <Col lg={12}>
                          <TextField
                            value={values.user_mobile}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            name="user_mobile"
                            margin="dense"
                            label="Mobile"
                            type="number"
                            fullWidth
                            variant="outlined"
                            onInput={(e) => {
                              e.target.value = Math.max(
                                0,
                                parseInt(e.target.value)
                              )
                                .toString()
                                .slice(0, 10);
                            }}
                            min={0}
                            error={errors.user_mobile && touched.user_mobile}
                            helperText={
                              errors.user_mobile &&
                              touched.user_mobile &&
                              errors.user_mobile
                            }
                          />
                        </Col>
                      </Row>
                    </Col>
                    <Row>
                      <Col lg={6} className="margin-zero-auto text-right mt-2">
                        <div>
                          <Button
                            onClick={this.handleRequestClose}
                            color="secondary"
                          >
                            Cancel
                          </Button>
                          {this.state.isEditable ? (
                            <Button type="submit" color="primary">
                              Update
                            </Button>
                          ) : (
                            <Button
                              type="button"
                              color="primary"
                              onClick={this.setEditable()}
                            >
                              Edit
                            </Button>
                          )}
                        </div>
                      </Col>
                    </Row>
                  </Container>
                </form>
              );
            }}
          </Formik>
        </div>
        {this.state.isLoading && (
          <div className="loader-view">
            <CircularProgress />
          </div>
        )}
        <NotificationContainer />
      </div>
    );
  }
}

export default index;
