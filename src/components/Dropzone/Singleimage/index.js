// import React, { Component } from 'react'
// import '../dropzone.scss'
// import AddBoxOutlinedIcon from '@material-ui/icons/AddBoxOutlined';
// import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
// import { Row, Col } from 'reactstrap';
// import { IconButton } from '@material-ui/core';

// export default class ImageDropZone extends Component {
//     constructor(props) {
//         super(props)
//         this.state = {
//             isMultiple: props.multiple || false,
//             files: [],
//             previewImages: [],
//         }
//     }
//     filehandler = event => {
//         let f = this.state.files;
//         f.push(...event.target.files)
//         this.setState({ files: f },() => {
//             this.props.onChange(this.state.files)
//         })
//     }

//     removeFile = (index) => {
//         let f = this.state.files;
//         f.splice(index, 1);
//         this.setState({ files: f },() => {
//             this.props.onChange(this.state.files)
//         });
//     }

//     render() {
//         const { files } = this.state
//         return (
//             <React.Fragment>
//                 <Row lg={3}>
//                     {(this.state.isMultiple ? true : this.state.files.length === 0) && <Col>
//                         <div className="add-image-container" style={{width: '100%',position: "relative", border: '2px dashed darkgray', borderRadius: '.5rem'}}>
//                             <input type="file" name="file" className="file-upload-input" style={{color: 'darkgray'}} multiple={this.state.isMultiple} onChange={this.filehandler} disabled={!this.state.isMultiple && this.state.files.length === 1} />
//                             <AddBoxOutlinedIcon className="w-100 h-100 text-muted" />
//                         </div>
//                     </Col>}
//                     {files.map((item, index) => (
//                         <Col key={index}>
//                             <div className="preview-image">
//                                 <img src={URL.createObjectURL(item)} className="img-fluid" alt="addimage" style={{padding: '5px',borderRadius: '10px'}}/>
//                                 <IconButton className="btn-remove" onClick={() => {this.removeFile(index)}}>
//                                     <DeleteForeverIcon fontSize="small"/>
//                                 </IconButton>
//                             </div>
//                         </Col>
//                     ))}
//                 </Row>
//             </React.Fragment>
//         )
//     }
// }

import React, { Component } from "react";
import "../dropzone.scss";
// import AddBoxOutlinedIcon from "@material-ui/icons/AddBoxOutlined";
// import DeleteForeverIcon from "@material-ui/icons/DeleteForever";
import { Row, Col } from "reactstrap";
import { Button, Tooltip } from "@material-ui/core";
import { Delete, GetApp } from "@material-ui/icons";
// import { IconButton } from "@material-ui/core";
import uploadimage from "../../../assets/images/uploadimagenew.png";
import pdf from "assets/images/pdf.png";
export default class ImageDropZone extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isMultiple: props.multiple || false,
      files: [],
      previewImages: [],
      errors: {},
    };
  }
  filehandler = (event) => {
    let validate = true;
    debugger;
    let f = this.state.files;
    f.push(...event.target.files);
    f.map((item, index) => {
      return [(validate = this.validation(item, index))];
    });
    if (validate) {
      this.setState({ files: f }, () => {
        this.props.onChange(this.state.files);
      });
    }
  };
  validation = (data, index) => {
    debugger;
    let errors = {};
    let formIsValid = true;
    if (data.type !== "image/jpeg") {
      if (data.type !== "image/png") {
        if (data.type !== "image/svg+xml") {
          if (data.type !== "application/pdf") {
            formIsValid = false;
            errors["Upload"] = "You can Only Upload JPG PNG Or PDF file";
            this.removeFile(index);
          }
        }
      }
    }
    this.setState({
      errors: errors,
    });
    return formIsValid;
  };
  removeFile = (index) => {
    let f = this.state.files;
    f.splice(index, 1);
    this.setState({ files: f }, () => {
      this.props.onChange(this.state.files);
    });
  };

  render() {
    const { files } = this.state;
    return (
      <React.Fragment>
        <div>
          <Row className="preview-image-upload mb-3">
            {files.map((item, index) => {
              debugger;
              switch (item.type) {
                case "image/jpeg":
                case "image/png":
                case "image/svg+xml":
                  return (
                    <Col
                      key={index}
                      lg={3}
                      md={3}
                      sm={6}
                      xs={6}
                      className="pr-0 text-center"
                    >
                      <div>
                        <img
                          src={URL.createObjectURL(item)}
                          className="img-fluid"
                          alt="addimage"
                          style={{ padding: "5px", borderRadius: "10px" }}
                        />
                        <div className="d-flex">
                          <a
                            href={URL.createObjectURL(item)}
                            download
                            target="_blank"
                            rel="noopener noreferrer"
                          >
                            <Button
                              color="primary"
                              variant="outlined"
                              // onClick={() => {
                              //   this.removeFile(index);
                              // }}
                            >
                              <Tooltip title={"Download"}>
                                <GetApp />
                              </Tooltip>
                            </Button>
                          </a>
                          <Button
                            color="primary"
                            variant="outlined"
                            onClick={() => {
                              this.removeFile(index);
                            }}
                          >
                            <Tooltip title={"Delete"}>
                              <Delete color="error" />
                            </Tooltip>
                          </Button>
                        </div>
                      </div>
                    </Col>
                  );
                case "application/pdf":
                  return (
                    <Col
                      key={index}
                      lg={3}
                      md={3}
                      sm={6}
                      xs={6}
                      className="pr-0 text-center"
                    >
                      <div>
                        <img
                          src={pdf}
                          className="img-fluid"
                          alt="addimage"
                          style={{ padding: "5px", borderRadius: "10px" }}
                        />
                        <div className="d-flex">
                          <a
                            href={URL.createObjectURL(item)}
                            download
                            target="_blank"
                            rel="noopener noreferrer"
                          >
                            <Button
                              color="primary"
                              variant="outlined"
                              // onClick={() => {
                              //   this.removeFile(index);
                              // }}
                            >
                              <Tooltip title={"Download"}>
                                <GetApp />
                              </Tooltip>
                            </Button>
                          </a>
                          <Button
                            color="primary"
                            variant="outlined"
                            onClick={() => {
                              this.removeFile(index);
                            }}
                          >
                            <Tooltip title={"Delete"}>
                              <Delete color="error" />
                            </Tooltip>
                          </Button>
                        </div>
                      </div>
                    </Col>
                  );
                default:
                  return null;
              }
            })}
          </Row>
          <Row>
            {(this.state.isMultiple ? true : this.state.files.length === 0) && (
              <Col>
                <div className="Dropzone">
                  <input
                    type="file"
                    name="file"
                    className="file-upload-input"
                    style={{ color: "darkgray" }}
                    multiple={this.state.isMultiple}
                    onChange={this.filehandler}
                    accept="image/*,.pdf"
                    disabled={
                      !this.state.isMultiple && this.state.files.length === 1
                    }
                  />

                  <div className="drag-text-two">
                    <img src={uploadimage} className="img-fluid" alt="upload" />
                  </div>

                  {/* <AddBoxOutlinedIcon className="w-100 h-100 text-muted" /> */}
                </div>
                {this.state.errors.Upload ? (
                  <div
                    className="errorMsg "
                    style={{
                      fontSize: "12px",
                      color: "red",
                      fontweight: "600",
                      marginTop: "20px",
                    }}
                  >
                    {this.state.errors.Upload}
                  </div>
                ) : null}
              </Col>
            )}
          </Row>
        </div>
      </React.Fragment>
    );
  }
}
