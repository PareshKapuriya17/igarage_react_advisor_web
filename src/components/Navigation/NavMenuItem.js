import React from 'react';
import {List} from '@material-ui/core';
import {NavLink} from 'react-router-dom'

const NavMenuItem = props => {
  const {name, icon, link} = props;

  return (
    <List component="div" className='nav-menu-item p-0'>
      <NavLink className="prepend-icon nav-menu-link" to={link}>
        {/* Display an icon if any */}
        {!!icon && (
          <i className={'zmdi zmdi-hc-fw  zmdi-' + icon}/>
        )}
        <span className="nav-text">{name}</span>
      </NavLink>
    </List>
  )
};

export default NavMenuItem;