import React from "react";
// import { getUser } from 'actions/Auth';
import ApiRequests from "util/Requests";
import { Row, Col, Container } from "reactstrap";
import {
  Button,
  TextField,
  CircularProgress,
  InputAdornment,
  IconButton,
} from "@material-ui/core";
// import PhotoCamera from '@material-ui/icons/PhotoCamera';
import "styles/change-password.scss";
import { Formik } from "formik";
import * as Yup from "yup";
import {
  NotificationContainer,
  NotificationManager,
} from "react-notifications";
import { userSignOut } from "actions/Auth";
import { connect } from "react-redux";
import { Visibility, VisibilityOff } from "@material-ui/icons";
// import { removeSession } from "util/Session";

export class index extends React.Component {
  constructor(props) {
    super(props);
    this.fileInputRef = React.createRef();
    this.state = {
      isLoading: false,
      data: {
        old_password: "",
        user_password: "",
        user_password_confirmation: "",
      },
      isEditable: false,
      imageUrl: "",
      showPassword: false,
      currentpassword: false,
      conformpassword: false,
    };
  }
  handleRequestClose = () => {
    this.props.history.push("/app/dashboard");
  };
  handleClickSubmit = (values) => {
    debugger;

    let formData = new FormData();
    formData.append("old_password", values.old_password);
    formData.append("user_password", values.user_password);
    formData.append(
      "user_password_confirmation",
      values.user_password_confirmation
    );

    this.setState({ isLoading: true });

    ApiRequests.changePassword(formData)
      .then((res) => {
        debugger;
        if (res.data.status === 1) {
          NotificationManager.success("Your Password changed!");
          this.setState({ isLoading: false });
          // removeSession();
          this.props.userSignOut();
          // this.getProfileData();
        } else {
          NotificationManager.error(res.data.message);
          this.setState({ isLoading: false });
        }
      })
      .catch((err) => {
        NotificationManager.error(err.response.data.message);
        this.setState({ pagination: { total: 0, page: 0, perPage: 10 } });
      });
  };
  handleClickShowPassword = () => {
    this.setState({
      showPassword: !this.state.showPassword,
    });
  };
  handleClickShowPasswordconform = () => {
    this.setState({
      conformpassword: !this.state.conformpassword,
    });
  };
  handleClickShowPasswordcurrent = () => {
    this.setState({
      currentpassword: !this.state.currentpassword,
    });
  };
  render() {
    return (
      <div className="card shadow text-center change-password-container m-5">
        <div className="change-password-div">
          <h1>Change Password</h1>

          <Formik
            enableReinitialize
            initialValues={{ ...this.state.data }}
            onSubmit={(event) => this.handleClickSubmit(event)}
            validationSchema={Yup.object().shape({
              old_password: Yup.string().required(
                "Please enter current password"
              ),
              user_password: Yup.string()
                .required("Please enter password")
                .min(6, "Password should be at least 6 character long"),
              // .matches(
              //   /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{4,}$/,
              //   "Must Contain 4 Characters, One Uppercase, One Lowercase, One Number and one special case Character"
              // ),
              user_password_confirmation: Yup.string()
                .required("Please confirm password")
                .test(
                  "passwords-match",
                  "Passwords and confirm password must match ",
                  function(value) {
                    return this.parent.user_password === value;
                  }
                ),
            })}
          >
            {(props) => {
              const {
                values,
                touched,
                errors,
                // dirty,
                handleChange,
                handleBlur,
                handleSubmit,
                // setFieldValue,
                // handleReset,
              } = props;
              return (
                <form onSubmit={handleSubmit}>
                  <Container>
                    <Col lg={6} className="margin-zero-auto">
                      <Row className="justify-content-md-center">
                        <Col lg={12}>
                          <TextField
                            value={values.old_password}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            name="old_password"
                            margin="dense"
                            label="Current Password"
                            type={
                              this.state.currentpassword ? "text" : "password"
                            }
                            fullWidth
                            required
                            InputProps={{
                              endAdornment: (
                                <InputAdornment position="end" className="mr-0">
                                  <IconButton
                                    aria-label="toggle password visibility"
                                    onClick={
                                      this.handleClickShowPasswordcurrent
                                    }
                                    className=""
                                  >
                                    {this.state.currentpassword ? (
                                      <Visibility fontSize="small" />
                                    ) : (
                                      <VisibilityOff fontSize="small" />
                                    )}
                                  </IconButton>
                                </InputAdornment>
                              ),
                            }}
                            variant="outlined"
                            error={errors.old_password && touched.old_password}
                            helperText={
                              errors.old_password &&
                              touched.old_password &&
                              errors.old_password
                            }
                          />
                        </Col>

                        <Col lg="12">
                          <TextField
                            value={values.user_password}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            name="user_password"
                            margin="dense"
                            label="New Password"
                            type={this.state.showPassword ? "text" : "password"}
                            fullWidth
                            required
                            InputProps={{
                              endAdornment: (
                                <InputAdornment position="end" className="mr-0">
                                  <IconButton
                                    aria-label="toggle password visibility"
                                    onClick={this.handleClickShowPassword}
                                    className=""
                                  >
                                    {this.state.showPassword ? (
                                      <Visibility fontSize="small" />
                                    ) : (
                                      <VisibilityOff fontSize="small" />
                                    )}
                                  </IconButton>
                                </InputAdornment>
                              ),
                            }}
                            variant="outlined"
                            error={
                              errors.user_password && touched.user_password
                            }
                            helperText={
                              errors.user_password &&
                              touched.user_password &&
                              errors.user_password
                            }
                          />
                        </Col>

                        <Col lg={12}>
                          <TextField
                            value={values.user_password_confirmation}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            name="user_password_confirmation"
                            margin="dense"
                            label="Confirm Password"
                            type={
                              this.state.conformpassword ? "text" : "password"
                            }
                            required
                            fullWidth
                            InputProps={{
                              endAdornment: (
                                <InputAdornment position="end" className="mr-0">
                                  <IconButton
                                    aria-label="toggle password visibility"
                                    onClick={
                                      this.handleClickShowPasswordconform
                                    }
                                    className=""
                                  >
                                    {this.state.conformpassword ? (
                                      <Visibility fontSize="small" />
                                    ) : (
                                      <VisibilityOff fontSize="small" />
                                    )}
                                  </IconButton>
                                </InputAdornment>
                              ),
                            }}
                            variant="outlined"
                            error={
                              errors.user_password_confirmation &&
                              touched.user_password_confirmation
                            }
                            helperText={
                              errors.user_password_confirmation &&
                              touched.user_password_confirmation &&
                              errors.user_password_confirmation
                            }
                          />
                        </Col>
                      </Row>
                    </Col>
                    <Row>
                      <Col lg={6} className="margin-zero-auto text-right mt-2">
                        <div>
                          <Button
                            onClick={this.handleRequestClose}
                            color="secondary"
                          >
                            Cancel
                          </Button>

                          <Button type="submit" color="primary">
                            Update
                          </Button>
                        </div>
                      </Col>
                    </Row>
                  </Container>
                </form>
              );
            }}
          </Formik>

          {this.state.isLoading && (
            <div className="loader-view">
              <CircularProgress />
            </div>
          )}
          <NotificationContainer />
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => state;
const mapDispatchToProps = {
  userSignOut,
};
export default connect(mapStateToProps, mapDispatchToProps)(index);
