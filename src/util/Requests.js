import { axiosInstance } from "./Api";

export const ApiRequests = {
  getOrderList: async (params) =>
    axiosInstance().post("mobile/order/advisor/list", params),
  getGarageList: async (str) =>
    axiosInstance().get(`/mobile/order/advisor/garage/search?${str}`),
  getMobileVanList: async (str) =>
    axiosInstance().get(`/mobile/vehicle/mobilevans?${str}`),
  assignGarageMobileVan: async (params) =>
    axiosInstance().post("/mobile/order/advisor/garage/assign", params),
  getOrderImages: async (id) =>
    axiosInstance().get(`/mobile/order/advisor/get/images?order_id=${id}`),
  orderPickupImages: async (params) =>
    axiosInstance().post(`/mobile/order/advisor/pickup`, params),
  estimateModify: async (params) =>
    axiosInstance().post(`/mobile/order/advisor/estimated/modify`, params),
  estimateSend: async (id) =>
    axiosInstance().get(`/mobile/order/advisor/estimated/send?order_id=${id}`),
  orderDeliveryImages: async (params) =>
    axiosInstance().post(`/mobile/order/advisor/delivery/image`, params),
  orderDelivered: async (id) =>
    axiosInstance().get(`/mobile/order/advisor/delivered?order_id=${id}`),
  orderCompletes: async (id) =>
    axiosInstance().get(`/mobile/order/advisor/completed?order_id=${id}`),
  timeSlotNotAvailable: async (params) =>
    axiosInstance().post(`/mobile/order/advisor/timeslot/notavailable`, params),
  orderCancel: async (params) =>
    axiosInstance().post(`/mobile/order/advisor/cancel`, params),
  getServiceList: async (str) =>
    axiosInstance().get(`/mobile/service/services?${str}`),
  getPackageList: async (str) => axiosInstance().get(`/mobile/packages`),
  updateEstimation: async (data) =>
    axiosInstance().post(`/mobile/order/advisor/estimated/modify`, data),
  sendEstimation: async (id) =>
    axiosInstance().get(`/mobile/order/advisor/estimated/send?order_id=${id}`),
  getUserProfile: async () => axiosInstance().get("web/adviser/auth/current"),
  updateProfile: async (data) =>
    axiosInstance().post(`mobile/user/advisor/profile/update`, data),
  imageGetApi: async (id) =>
    axiosInstance().get(`mobile/order/garage/get/images?order_id=${id}`),
  changePassword: async (data) =>
    axiosInstance().post(`mobile/user/advisor/change/password`, data),
  getChatHistory: async (params) =>
    axiosInstance().get(`/mobile/chat/history?${params}`),
  uploadChatFile: async (data) =>
    axiosInstance().post(`/mobile/chat/file/upload`, data),
  downloaddocument: async (data) =>
    axiosInstance().get(`download?file=${data}`),
};

export default ApiRequests;
