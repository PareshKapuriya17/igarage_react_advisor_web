import Ws from "@adonisjs/websocket-client";
import { AppSettings } from "constants/AppSettings";
import { getSession } from "./Session";
let subscribestatuscustumer = false;
let subscribestatusgarage = false;
export class WebSocket {
  connect() {
    const token = getSession("token");
    this.ws = Ws(`${AppSettings.SOCKET_BASE_URL}`)
      .withApiToken(token)
      .connect();
    this.ws.on("open", () => {
      console.log("Connection initialized");
    });

    this.ws.on("close", () => {
      console.log("Connection closed");
    });

    return this;
  }

  subscribe(channel, handler, roll) {
    if (!this.ws) {
      debugger;
      subscribestatuscustumer = false;
      setTimeout(() => this.subscribe(channel, handler, roll), 1000);
    } else {
      debugger;
      if (subscribestatuscustumer) {
        const subscription = this.ws.getSubscription(channel);
        if (subscription === undefined) {
          subscribestatuscustumer = false;
          this.subscribe(channel, handler, roll);
        } else {
          subscription.on("message", (message) => {
            console.log("Incoming", message);
            handler({ type: "newMessage", data: message });
          });
          subscription.on("error", (error) => {
            console.error(error);
          });
          return subscription;
        }
      } else {
        const result = this.ws.subscribe(channel);
        console.log(result);
        subscribestatuscustumer = true;
        result.on("message", (message) => {
          console.log("Incoming", message);
          handler({ type: "newMessage", data: message });
        });
        result.on("error", (error) => {
          console.error(error);
        });
        return result;
      }
    }
  }
  subscribegarage(channel, handler, roll) {
    if (!this.ws) {
      debugger;
      subscribestatusgarage = false;
      setTimeout(() => this.subscribegarage(channel, handler, roll), 1000);
    } else {
      if (subscribestatusgarage) {
        const subscription = this.ws.getSubscription(channel);
        if (subscription === undefined) {
          subscribestatusgarage = false;
          this.subscribegarage(channel, handler, roll);
        } else {
          subscription.on("message", (message) => {
            console.log("Incoming", message);
            handler({ type: "newMessage", data: message });
          });
          subscription.on("error", (error) => {
            console.error(error);
          });
          return subscription;
        }
      } else {
        const result = this.ws.subscribe(channel);
        console.log(result);
        subscribestatusgarage = true;
        result.on("message", (message) => {
          console.log("Incoming", message);
          handler({ type: "newMessage", data: message });
        });
        result.on("error", (error) => {
          console.error(error);
        });
        return result;
      }
    }
  }

  // subscribe(channel, handler, roll) {
  //   if (!this.ws) {
  //     debugger;
  //     setTimeout(() => this.subscribe(channel, handler, roll), 1000);
  //   } else {
  //     debugger;
  //     if (roll === "customer") {
  //       if (subscribestatuscustumer) {
  //         const subscription = this.ws.getSubscription(channel);
  //         if (subscription._state === undefined) {
  //           this.subscribe(channel);
  //           subscribestatuscustumer = false;
  //         } else {
  //           subscription.on("message", (message) => {
  //             console.log("Incoming", message);
  //             handler({ type: "newMessage", data: message });
  //           });
  //           subscription.on("error", (error) => {
  //             console.error(error);
  //           });
  //           return subscription;
  //         }
  //       } else {
  //         const result = this.ws.subscribe(channel);
  //         console.log(result);
  //         subscribestatuscustumer = true;
  //         result.on("message", (message) => {
  //           console.log("Incoming", message);
  //           handler({ type: "newMessage", data: message });
  //         });
  //         result.on("error", (error) => {
  //           console.error(error);
  //         });
  //         return result;
  //       }
  //     } else if (roll === "garage") {
  //       if (subscribestatusgarage) {
  //         const subscription = this.ws.getSubscription(channel);
  //         if (subscription._state === undefined) {
  //           this.subscribe(channel);
  //           subscribestatusgarage = false;
  //         } else {
  //           subscription.on("message", (message) => {
  //             console.log("Incoming", message);
  //             handler({ type: "newMessage", data: message });
  //           });
  //           subscription.on("error", (error) => {
  //             console.error(error);
  //           });
  //           return subscription;
  //         }
  //       } else {
  //         const result = this.ws.subscribe(channel);
  //         console.log(result);

  //         subscribestatusgarage = true;
  //         result.on("message", (message) => {
  //           console.log("Incoming", message);
  //           handler({ type: "newMessage", data: message });
  //         });
  //         result.on("error", (error) => {
  //           console.error(error);
  //         });
  //         return result;
  //       }
  //     } else {
  //       const result = this.ws.subscribe(channel);
  //       console.log(result);
  //       result.on("message", (message) => {
  //         console.log("Incoming", message);
  //         handler({ type: "newMessage", data: message });
  //       });
  //       result.on("error", (error) => {
  //         console.error(error);
  //       });
  //       return result;
  //     }
  //   }
  // }
}

export default new WebSocket();
