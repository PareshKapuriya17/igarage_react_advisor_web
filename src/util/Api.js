import axios from "axios";
import { NotificationManager } from "react-notifications";
import { userSignOut } from "actions";
import { AppSettings } from "constants/AppSettings";
import { getSession } from "./Session";
import { store } from "MainApp";

export const axiosInstance = () => {
  const instance = axios.create({
    baseURL: AppSettings.API_BASE_URL,
    headers: {
      "Content-Type": "application/json",
    },
  });

  instance.interceptors.request.use(
    (config) => {
      debugger;
      var token = getSession("token");
      config.headers["Authorization"] = "Bearer " + token;
      return config;
    },
    (error) => {
      return Promise.reject(error);
    }
  );

  instance.interceptors.response.use(
    (response) => {
      debugger;
      if (response.data.status !== -1) {
        return response;
      } else {
        store.dispatch(userSignOut());
        return response;
      }
    },
    (error) => {
      console.log(toString(error));
      debugger;
      if (error.Error === undefined) {
        console.log("----SESSION----");
        console.log(error);
        store.dispatch(userSignOut());
      } else if (
        error.response.data.status === -1 &&
        error.response.data.message === "Invalid Token"
      ) {
        console.log("----SESSION----");
        console.log(error);
        store.dispatch(userSignOut());
        NotificationManager.warning("Session expired. Please login again!");
      } else {
        const msg = error.response
          ? error.response.data.message
          : error.message;
        NotificationManager.error(msg);
      }
      return Promise.reject(error);
    }
  );

  return instance;
};
